(define (problem BLOCKS-15-1)
(:domain BLOCKS)
(:objects J B K A D H E N C F L M I O G )
(:INIT (CLEAR G) (CLEAR O) (ONTABLE I) (ONTABLE M) (ON G L) (ON L F) (ON F C)
 (ON C N) (ON N E) (ON E H) (ON H D) (ON D A) (ON A K) (ON K B) (ON B J)
 (ON J I) (ON O M) (HANDEMPTY))
 (:utility 
    (= (ON D G) 1 ) 
    (= (ON G F) 1 ) 
    (= (ON F K) 1 ) 
    (= (ON K J) 1 ) 
    (= (ON J E) 1 ) 
    (= (ON E M) 1 ) 
    (= (ON M A) 1 ) 
    (= (ON A B) 1 ) 
    (= (ON B C) 1 ) 
    (= (ON C N) 1 ) 
    (= (ON N O) 1 ) 
    (= (ON O I) 1 ) 
    (= (ON I L) 1 ) 
    (= (ON L H) 1 ) 
    (= (on k c) 1 ) 
    (= (on f c) 1 ) 
    (= (on c l) 1 ) 
    (= (on d o) 1 ) 
    (= (on d f) 1 ) 
    (= (on o b) 1 ) 
    (= (on g a) 1 ) 
    (= (on a b) 1 ) 
    (= (on e g) 1 ) 
    (= (ontable j) 1 ) 
    (= (holding b) 1 ) 
    (= (ontable b) 1 ) 
    (= (on i e) 1 ) 
 )
 (:bound 66)
 )
 


