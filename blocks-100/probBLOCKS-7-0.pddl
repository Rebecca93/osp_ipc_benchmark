(define (problem BLOCKS-7-0)
(:domain BLOCKS)
(:objects C F A B G D E )
(:INIT (CLEAR E) (ONTABLE D) (ON E G) (ON G B) (ON B A) (ON A F) (ON F C)
 (ON C D) (HANDEMPTY))
 (:utility 
    (= (ON A G) 1 ) 
    (= (ON G D) 1 ) 
    (= (ON D B) 1 ) 
    (= (ON B C) 1 ) 
    (= (ON C F) 1 ) 
    (= (ON F E) 1 ) 
    (= (on g f) 1 ) 
    (= (on c d) 1 ) 
    (= (on e b) 1 ) 
 )
 (:bound 20)
 )
 


