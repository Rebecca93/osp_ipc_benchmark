(define (problem BLOCKS-8-2)
(:domain BLOCKS)
(:objects F B G C H E A D )
(:INIT (CLEAR D) (CLEAR A) (CLEAR E) (CLEAR H) (CLEAR C) (ONTABLE G)
 (ONTABLE A) (ONTABLE E) (ONTABLE H) (ONTABLE C) (ON D B) (ON B F) (ON F G)
 (HANDEMPTY))
 (:utility 
    (= (ON C B) 1 ) 
    (= (ON B E) 1 ) 
    (= (ON E G) 1 ) 
    (= (ON G F) 1 ) 
    (= (ON F A) 1 ) 
    (= (ON A D) 1 ) 
    (= (ON D H) 1 ) 
    (= (on g h) 1 ) 
    (= (on h g) 1 ) 
    (= (clear e) 1 ) 
    (= (holding d) 1 ) 
 )
 (:bound 16)
 )
 


