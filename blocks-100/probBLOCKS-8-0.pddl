(define (problem BLOCKS-8-0)
(:domain BLOCKS)
(:objects H G F E C B D A )
(:INIT (CLEAR A) (CLEAR D) (CLEAR B) (CLEAR C) (ONTABLE E) (ONTABLE F)
 (ONTABLE B) (ONTABLE C) (ON A G) (ON G E) (ON D H) (ON H F) (HANDEMPTY))
 (:utility 
    (= (ON D F) 1 ) 
    (= (ON F E) 1 ) 
    (= (ON E H) 1 ) 
    (= (ON H C) 1 ) 
    (= (ON C A) 1 ) 
    (= (ON A G) 1 ) 
    (= (ON G B) 1 ) 
    (= (holding g) 1 ) 
    (= (ontable c) 1 ) 
    (= (holding d) 1 ) 
    (= (on a g) 1 ) 
 )
 (:bound 18)
 )
 


