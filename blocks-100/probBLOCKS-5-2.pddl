(define (problem BLOCKS-5-2)
(:domain BLOCKS)
(:objects A C E B D )
(:INIT (CLEAR D) (ONTABLE B) (ON D E) (ON E C) (ON C A) (ON A B) (HANDEMPTY))
 (:utility 
    (= (ON D C) 1 ) 
    (= (ON C B) 1 ) 
    (= (ON B E) 1 ) 
    (= (ON E A) 1 ) 
    (= (on c a) 1 ) 
    (= (holding a) 1 ) 
 )
 (:bound 16)
 )
 


