(define (problem BLOCKS-4-0)
(:domain BLOCKS)
(:objects D B A C )
(:INIT (CLEAR C) (CLEAR A) (CLEAR B) (CLEAR D) (ONTABLE C) (ONTABLE A)
 (ONTABLE B) (ONTABLE D) (HANDEMPTY))
 (:utility 
    (= (ON D C) 1 ) 
    (= (ON C B) 1 ) 
    (= (ON B A) 1 ) 
    (= (on c b) 1 ) 
 )
 (:bound 6)
 )
 


