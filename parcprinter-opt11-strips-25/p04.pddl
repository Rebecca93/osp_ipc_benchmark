(define (problem PrintJob)
(:domain upp)
(:objects
		dummy-sheet
		sheet1
		sheet2
		sheet3
		sheet4 - sheet_t
		image-1
		image-2
		image-3
		image-4 - image_t
)
(:init
		(Uninitialized)
		(= (total-cost) 1 )
		(Oppositeside Front Back)
		(Oppositeside Back Front)
		(Location dummy-sheet Some_Finisher_Tray)
		(Prevsheet sheet1 dummy-sheet)
		(Prevsheet sheet2 sheet1)
		(Prevsheet sheet3 sheet2)
		(Prevsheet sheet4 sheet3)
		(Sheetsize sheet1 Letter)
		(Sheetsize sheet2 Letter)
		(Sheetsize sheet3 Letter)
		(Sheetsize sheet4 Letter)
		(Location sheet1 Some_Feeder_Tray)
		(Location sheet2 Some_Feeder_Tray)
		(Location sheet3 Some_Feeder_Tray)
		(Location sheet4 Some_Feeder_Tray)
		(Imagecolor image-1 Black)
		(Imagecolor image-2 Black)
		(Imagecolor image-3 Color)
		(Imagecolor image-4 Color)
		(Notprintedwith sheet1 Front Black)
		(Notprintedwith sheet1 Back Black)
		(Notprintedwith sheet1 Front Color)
		(Notprintedwith sheet1 Back Color)
		(Notprintedwith sheet2 Front Black)
		(Notprintedwith sheet2 Back Black)
		(Notprintedwith sheet2 Front Color)
		(Notprintedwith sheet2 Back Color)
		(Notprintedwith sheet3 Front Black)
		(Notprintedwith sheet3 Back Black)
		(Notprintedwith sheet3 Front Color)
		(Notprintedwith sheet3 Back Color)
		(Notprintedwith sheet4 Front Black)
		(Notprintedwith sheet4 Back Black)
		(Notprintedwith sheet4 Front Color)
		(Notprintedwith sheet4 Back Color)
)
 (:utility 
    (= (Hasimage sheet1 Front image-1) 1 ) 
    (= (Notprintedwith sheet1 Front Color) 1 ) 
    (= (Notprintedwith sheet1 Back Black) 1 ) 
    (= (Notprintedwith sheet1 Back Color) 1 ) 
    (= (Hasimage sheet2 Front image-2) 1 ) 
    (= (Notprintedwith sheet2 Front Color) 1 ) 
    (= (Notprintedwith sheet2 Back Black) 1 ) 
    (= (Notprintedwith sheet2 Back Color) 1 ) 
    (= (Hasimage sheet3 Front image-3) 1 ) 
    (= (Notprintedwith sheet3 Front Black) 1 ) 
    (= (Notprintedwith sheet3 Back Black) 1 ) 
    (= (Notprintedwith sheet3 Back Color) 1 ) 
    (= (Hasimage sheet4 Front image-4) 1 ) 
    (= (Notprintedwith sheet4 Front Black) 1 ) 
    (= (Notprintedwith sheet4 Back Black) 1 ) 
    (= (Notprintedwith sheet4 Back Color) 1 ) 
    (= (Sideup sheet1 Front) 1 ) 
    (= (Sideup sheet2 Front) 1 ) 
    (= (Sideup sheet3 Front) 1 ) 
    (= (Sideup sheet4 Front) 1 ) 
    (= (Stackedin sheet1 Finisher1_Tray) 1 ) 
    (= (Stackedin sheet2 Finisher1_Tray) 1 ) 
    (= (Stackedin sheet3 Finisher1_Tray) 1 ) 
    (= (Stackedin sheet4 Finisher1_Tray) 1 ) 
    (= (location sheet1 blackcontainer_exittoime-blackprinter_entry) 1 ) 
    (= (location sheet3 endcap_entry-blackcontainer_exit) 1 ) 
    (= (location sheet2 colorcontainer_entry-down_bottomexit) 1 ) 
    (= (hasimage sheet3 back image-4) 1 ) 
    (= (sideup sheet3 front) 1 ) 
    (= (hasimage sheet2 front image-2) 1 ) 
    (= (hasimage sheet4 front image-4) 1 ) 
 )
 (:bound 219023)
 (:use-cost-metric)
 )
 
 


