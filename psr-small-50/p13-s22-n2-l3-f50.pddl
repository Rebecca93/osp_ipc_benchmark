(define (problem grounded-STRIPS-PSR-S22-N2-L3-F50)
(:domain grounded-STRIPS-PSR)
(:init
(do-normal)
(NOT-UPDATED-CB1)
(NOT-UPDATED-CB2)
(NOT-CLOSED-SD1)
(NOT-CLOSED-SD3)
(CLOSED-SD6)
(CLOSED-SD5)
(CLOSED-SD4)
(CLOSED-SD2)
(CLOSED-CB2)
(CLOSED-CB1)
)
 (:utility 
    (= (do-normal) 1 ) 
    (= (CLOSED-SD1) 1 ) 
    (= (CLOSED-CB1) 1 ) 
    (= (UPDATED-CB1) 1 ) 
    (= (UPDATED-CB2) 1 ) 
    (= (not-closed-sd1) 1 ) 
 )
 (:bound 7)
 )
 


