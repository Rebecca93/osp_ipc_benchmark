(define (problem grounded-STRIPS-PSR-S12-N1-L5-F10)
(:domain grounded-STRIPS-PSR)
(:init
(do-normal)
(NOT-UPDATED-CB1)
(CLOSED-SD5)
(CLOSED-SD4)
(CLOSED-SD3)
(CLOSED-SD2)
(CLOSED-SD1)
(CLOSED-CB1)
)
 (:utility 
    (= (do-normal) 1 ) 
    (= (CLOSED-SD5) 1 ) 
    (= (CLOSED-SD2) 1 ) 
    (= (CLOSED-SD1) 1 ) 
    (= (CLOSED-CB1) 1 ) 
    (= (CLOSED-SD4) 1 ) 
    (= (UPDATED-CB1) 1 ) 
 )
 (:bound 4)
 )
 


