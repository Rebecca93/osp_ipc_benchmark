(define (problem grounded-STRIPS-PSR-S5-N1-L3-F30)
(:domain grounded-STRIPS-PSR)
(:init
(do-normal)
(NOT-UPDATED-CB1)
(CLOSED-SD8)
(CLOSED-SD7)
(CLOSED-SD6)
(CLOSED-SD5)
(CLOSED-SD4)
(CLOSED-SD3)
(CLOSED-SD2)
(CLOSED-SD1)
(CLOSED-CB1)
)
 (:utility 
    (= (do-normal) 1 ) 
    (= (CLOSED-SD7) 1 ) 
    (= (CLOSED-CB1) 1 ) 
    (= (UPDATED-CB1) 1 ) 
    (= (updated-cb1) 1 ) 
 )
 (:bound 5)
 )
 


