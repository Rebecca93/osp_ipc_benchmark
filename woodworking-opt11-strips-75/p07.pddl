; woodworking task with 6 parts and 120% wood
; Machines:
;   1 grinder
;   1 glazer
;   1 immersion-varnisher
;   1 planer
;   1 highspeed-saw
;   1 spray-varnisher
;   1 saw
; random seed: 709405

(define (problem wood-prob)
  (:domain woodworking)
  (:objects
    grinder0 - grinder
    glazer0 - glazer
    immersion-varnisher0 - immersion-varnisher
    planer0 - planer
    highspeed-saw0 - highspeed-saw
    spray-varnisher0 - spray-varnisher
    saw0 - saw
    mauve red white black - acolour
    oak teak - awood
    p0 p1 p2 p3 p4 p5 - part
    b0 b1 - board
    s0 s1 s2 s3 s4 s5 s6 s7 s8 - aboardsize
  )
  (:init
    (grind-treatment-change varnished colourfragments)
    (grind-treatment-change glazed untreated)
    (grind-treatment-change untreated untreated)
    (grind-treatment-change colourfragments untreated)
    (is-smooth smooth)
    (is-smooth verysmooth)
    (= (total-cost) 1 )
    (boardsize-successor s0 s1)
    (boardsize-successor s1 s2)
    (boardsize-successor s2 s3)
    (boardsize-successor s3 s4)
    (boardsize-successor s4 s5)
    (boardsize-successor s5 s6)
    (boardsize-successor s6 s7)
    (boardsize-successor s7 s8)
    (has-colour glazer0 mauve)
    (has-colour glazer0 natural)
    (has-colour immersion-varnisher0 mauve)
    (has-colour immersion-varnisher0 natural)
    (empty highspeed-saw0)
    (has-colour spray-varnisher0 mauve)
    (has-colour spray-varnisher0 natural)
    (unused p0)
    (goalsize p0 large)
    (= (spray-varnish-cost p0) 1 )
    (= (glaze-cost p0) 1 )
    (= (grind-cost p0) 1 )
    (= (plane-cost p0) 1 )
    (unused p1)
    (goalsize p1 small)
    (= (spray-varnish-cost p1) 1 )
    (= (glaze-cost p1) 1 )
    (= (grind-cost p1) 1 )
    (= (plane-cost p1) 1 )
    (unused p2)
    (goalsize p2 small)
    (= (spray-varnish-cost p2) 1 )
    (= (glaze-cost p2) 1 )
    (= (grind-cost p2) 1 )
    (= (plane-cost p2) 1 )
    (unused p3)
    (goalsize p3 small)
    (= (spray-varnish-cost p3) 1 )
    (= (glaze-cost p3) 1 )
    (= (grind-cost p3) 1 )
    (= (plane-cost p3) 1 )
    (unused p4)
    (goalsize p4 medium)
    (= (spray-varnish-cost p4) 1 )
    (= (glaze-cost p4) 1 )
    (= (grind-cost p4) 1 )
    (= (plane-cost p4) 1 )
    (unused p5)
    (goalsize p5 small)
    (= (spray-varnish-cost p5) 1 )
    (= (glaze-cost p5) 1 )
    (= (grind-cost p5) 1 )
    (= (plane-cost p5) 1 )
    (boardsize b0 s8)
    (wood b0 teak)
    (surface-condition b0 rough)
    (available b0)
    (boardsize b1 s4)
    (wood b1 oak)
    (surface-condition b1 smooth)
    (available b1)
  )
 (:utility 
    (= (available p0) 1 ) 
    (= (wood p0 teak) 1 ) 
    (= (surface-condition p0 smooth) 1 ) 
    (= (available p1) 1 ) 
    (= (colour p1 natural) 1 ) 
    (= (wood p1 teak) 1 ) 
    (= (surface-condition p1 verysmooth) 1 ) 
    (= (available p2) 1 ) 
    (= (wood p2 oak) 1 ) 
    (= (surface-condition p2 verysmooth) 1 ) 
    (= (treatment p2 varnished) 1 ) 
    (= (available p3) 1 ) 
    (= (wood p3 oak) 1 ) 
    (= (surface-condition p3 smooth) 1 ) 
    (= (available p4) 1 ) 
    (= (colour p4 mauve) 1 ) 
    (= (wood p4 teak) 1 ) 
    (= (available p5) 1 ) 
    (= (colour p5 natural) 1 ) 
    (= (wood p5 oak) 1 ) 
    (= (surface-condition p5 smooth) 1 ) 
    (= (treatment p5 varnished) 1 ) 
    (= (surface-condition p2 rough) 1 ) 
    (= (boardsize b1 s2) 1 ) 
    (= (unused p5) 1 ) 
 )
 (:bound 168)
 (:use-cost-metric)
 )
 
 


  
