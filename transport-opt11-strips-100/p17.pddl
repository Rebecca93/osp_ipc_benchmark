; Transport city-sequential-15nodes-1000size-3degree-100mindistance-2trucks-6packages-2008seed

(define (problem transport-city-sequential-15nodes-1000size-3degree-100mindistance-2trucks-6packages-2008seed)
 (:domain transport)
 (:objects
  city-loc-1 - location
  city-loc-2 - location
  city-loc-3 - location
  city-loc-4 - location
  city-loc-5 - location
  city-loc-6 - location
  city-loc-7 - location
  city-loc-8 - location
  city-loc-9 - location
  city-loc-10 - location
  city-loc-11 - location
  city-loc-12 - location
  city-loc-13 - location
  city-loc-14 - location
  city-loc-15 - location
  truck-1 - vehicle
  truck-2 - vehicle
  package-1 - package
  package-2 - package
  package-3 - package
  package-4 - package
  package-5 - package
  package-6 - package
  capacity-0 - capacity-number
  capacity-1 - capacity-number
  capacity-2 - capacity-number
  capacity-3 - capacity-number
  capacity-4 - capacity-number
 )
 (:init
  (= (total-cost) 1 )
  (capacity-predecessor capacity-0 capacity-1)
  (capacity-predecessor capacity-1 capacity-2)
  (capacity-predecessor capacity-2 capacity-3)
  (capacity-predecessor capacity-3 capacity-4)
  ; 347,149 -> 257,5
  (road city-loc-4 city-loc-1)
  (= (road-length city-loc-4 city-loc-1) 1 )
  ; 257,5 -> 347,149
  (road city-loc-1 city-loc-4)
  (= (road-length city-loc-1 city-loc-4) 1 )
  ; 347,149 -> 245,346
  (road city-loc-4 city-loc-2)
  (= (road-length city-loc-4 city-loc-2) 1 )
  ; 245,346 -> 347,149
  (road city-loc-2 city-loc-4)
  (= (road-length city-loc-2 city-loc-4) 1 )
  ; 336,475 -> 245,346
  (road city-loc-5 city-loc-2)
  (= (road-length city-loc-5 city-loc-2) 1 )
  ; 245,346 -> 336,475
  (road city-loc-2 city-loc-5)
  (= (road-length city-loc-2 city-loc-5) 1 )
  ; 336,475 -> 559,565
  (road city-loc-5 city-loc-3)
  (= (road-length city-loc-5 city-loc-3) 1 )
  ; 559,565 -> 336,475
  (road city-loc-3 city-loc-5)
  (= (road-length city-loc-3 city-loc-5) 1 )
  ; 170,709 -> 336,475
  (road city-loc-6 city-loc-5)
  (= (road-length city-loc-6 city-loc-5) 1 )
  ; 336,475 -> 170,709
  (road city-loc-5 city-loc-6)
  (= (road-length city-loc-5 city-loc-6) 1 )
  ; 521,375 -> 245,346
  (road city-loc-7 city-loc-2)
  (= (road-length city-loc-7 city-loc-2) 1 )
  ; 245,346 -> 521,375
  (road city-loc-2 city-loc-7)
  (= (road-length city-loc-2 city-loc-7) 1 )
  ; 521,375 -> 559,565
  (road city-loc-7 city-loc-3)
  (= (road-length city-loc-7 city-loc-3) 1 )
  ; 559,565 -> 521,375
  (road city-loc-3 city-loc-7)
  (= (road-length city-loc-3 city-loc-7) 1 )
  ; 521,375 -> 347,149
  (road city-loc-7 city-loc-4)
  (= (road-length city-loc-7 city-loc-4) 1 )
  ; 347,149 -> 521,375
  (road city-loc-4 city-loc-7)
  (= (road-length city-loc-4 city-loc-7) 1 )
  ; 521,375 -> 336,475
  (road city-loc-7 city-loc-5)
  (= (road-length city-loc-7 city-loc-5) 1 )
  ; 336,475 -> 521,375
  (road city-loc-5 city-loc-7)
  (= (road-length city-loc-5 city-loc-7) 1 )
  ; 720,241 -> 521,375
  (road city-loc-9 city-loc-7)
  (= (road-length city-loc-9 city-loc-7) 1 )
  ; 521,375 -> 720,241
  (road city-loc-7 city-loc-9)
  (= (road-length city-loc-7 city-loc-9) 1 )
  ; 720,241 -> 701,0
  (road city-loc-9 city-loc-8)
  (= (road-length city-loc-9 city-loc-8) 1 )
  ; 701,0 -> 720,241
  (road city-loc-8 city-loc-9)
  (= (road-length city-loc-8 city-loc-9) 1 )
  ; 630,722 -> 559,565
  (road city-loc-10 city-loc-3)
  (= (road-length city-loc-10 city-loc-3) 1 )
  ; 559,565 -> 630,722
  (road city-loc-3 city-loc-10)
  (= (road-length city-loc-3 city-loc-10) 1 )
  ; 120,854 -> 170,709
  (road city-loc-11 city-loc-6)
  (= (road-length city-loc-11 city-loc-6) 1 )
  ; 170,709 -> 120,854
  (road city-loc-6 city-loc-11)
  (= (road-length city-loc-6 city-loc-11) 1 )
  ; 377,283 -> 245,346
  (road city-loc-12 city-loc-2)
  (= (road-length city-loc-12 city-loc-2) 1 )
  ; 245,346 -> 377,283
  (road city-loc-2 city-loc-12)
  (= (road-length city-loc-2 city-loc-12) 1 )
  ; 377,283 -> 347,149
  (road city-loc-12 city-loc-4)
  (= (road-length city-loc-12 city-loc-4) 1 )
  ; 347,149 -> 377,283
  (road city-loc-4 city-loc-12)
  (= (road-length city-loc-4 city-loc-12) 1 )
  ; 377,283 -> 336,475
  (road city-loc-12 city-loc-5)
  (= (road-length city-loc-12 city-loc-5) 1 )
  ; 336,475 -> 377,283
  (road city-loc-5 city-loc-12)
  (= (road-length city-loc-5 city-loc-12) 1 )
  ; 377,283 -> 521,375
  (road city-loc-12 city-loc-7)
  (= (road-length city-loc-12 city-loc-7) 1 )
  ; 521,375 -> 377,283
  (road city-loc-7 city-loc-12)
  (= (road-length city-loc-7 city-loc-12) 1 )
  ; 171,545 -> 245,346
  (road city-loc-13 city-loc-2)
  (= (road-length city-loc-13 city-loc-2) 1 )
  ; 245,346 -> 171,545
  (road city-loc-2 city-loc-13)
  (= (road-length city-loc-2 city-loc-13) 1 )
  ; 171,545 -> 336,475
  (road city-loc-13 city-loc-5)
  (= (road-length city-loc-13 city-loc-5) 1 )
  ; 336,475 -> 171,545
  (road city-loc-5 city-loc-13)
  (= (road-length city-loc-5 city-loc-13) 1 )
  ; 171,545 -> 170,709
  (road city-loc-13 city-loc-6)
  (= (road-length city-loc-13 city-loc-6) 1 )
  ; 170,709 -> 171,545
  (road city-loc-6 city-loc-13)
  (= (road-length city-loc-6 city-loc-13) 1 )
  ; 348,607 -> 245,346
  (road city-loc-14 city-loc-2)
  (= (road-length city-loc-14 city-loc-2) 1 )
  ; 245,346 -> 348,607
  (road city-loc-2 city-loc-14)
  (= (road-length city-loc-2 city-loc-14) 1 )
  ; 348,607 -> 559,565
  (road city-loc-14 city-loc-3)
  (= (road-length city-loc-14 city-loc-3) 1 )
  ; 559,565 -> 348,607
  (road city-loc-3 city-loc-14)
  (= (road-length city-loc-3 city-loc-14) 1 )
  ; 348,607 -> 336,475
  (road city-loc-14 city-loc-5)
  (= (road-length city-loc-14 city-loc-5) 1 )
  ; 336,475 -> 348,607
  (road city-loc-5 city-loc-14)
  (= (road-length city-loc-5 city-loc-14) 1 )
  ; 348,607 -> 170,709
  (road city-loc-14 city-loc-6)
  (= (road-length city-loc-14 city-loc-6) 1 )
  ; 170,709 -> 348,607
  (road city-loc-6 city-loc-14)
  (= (road-length city-loc-6 city-loc-14) 1 )
  ; 348,607 -> 521,375
  (road city-loc-14 city-loc-7)
  (= (road-length city-loc-14 city-loc-7) 1 )
  ; 521,375 -> 348,607
  (road city-loc-7 city-loc-14)
  (= (road-length city-loc-7 city-loc-14) 1 )
  ; 348,607 -> 171,545
  (road city-loc-14 city-loc-13)
  (= (road-length city-loc-14 city-loc-13) 1 )
  ; 171,545 -> 348,607
  (road city-loc-13 city-loc-14)
  (= (road-length city-loc-13 city-loc-14) 1 )
  ; 395,741 -> 559,565
  (road city-loc-15 city-loc-3)
  (= (road-length city-loc-15 city-loc-3) 1 )
  ; 559,565 -> 395,741
  (road city-loc-3 city-loc-15)
  (= (road-length city-loc-3 city-loc-15) 1 )
  ; 395,741 -> 336,475
  (road city-loc-15 city-loc-5)
  (= (road-length city-loc-15 city-loc-5) 1 )
  ; 336,475 -> 395,741
  (road city-loc-5 city-loc-15)
  (= (road-length city-loc-5 city-loc-15) 1 )
  ; 395,741 -> 170,709
  (road city-loc-15 city-loc-6)
  (= (road-length city-loc-15 city-loc-6) 1 )
  ; 170,709 -> 395,741
  (road city-loc-6 city-loc-15)
  (= (road-length city-loc-6 city-loc-15) 1 )
  ; 395,741 -> 630,722
  (road city-loc-15 city-loc-10)
  (= (road-length city-loc-15 city-loc-10) 1 )
  ; 630,722 -> 395,741
  (road city-loc-10 city-loc-15)
  (= (road-length city-loc-10 city-loc-15) 1 )
  ; 395,741 -> 120,854
  (road city-loc-15 city-loc-11)
  (= (road-length city-loc-15 city-loc-11) 1 )
  ; 120,854 -> 395,741
  (road city-loc-11 city-loc-15)
  (= (road-length city-loc-11 city-loc-15) 1 )
  ; 395,741 -> 171,545
  (road city-loc-15 city-loc-13)
  (= (road-length city-loc-15 city-loc-13) 1 )
  ; 171,545 -> 395,741
  (road city-loc-13 city-loc-15)
  (= (road-length city-loc-13 city-loc-15) 1 )
  ; 395,741 -> 348,607
  (road city-loc-15 city-loc-14)
  (= (road-length city-loc-15 city-loc-14) 1 )
  ; 348,607 -> 395,741
  (road city-loc-14 city-loc-15)
  (= (road-length city-loc-14 city-loc-15) 1 )
  (at package-1 city-loc-10)
  (at package-2 city-loc-11)
  (at package-3 city-loc-5)
  (at package-4 city-loc-9)
  (at package-5 city-loc-6)
  (at package-6 city-loc-4)
  (at truck-1 city-loc-12)
  (capacity truck-1 capacity-3)
  (at truck-2 city-loc-5)
  (capacity truck-2 capacity-2)
 )
 (:utility 
    (= (at package-1 city-loc-13) 1 ) 
    (= (at package-2 city-loc-3) 1 ) 
    (= (at package-3 city-loc-2) 1 ) 
    (= (at package-4 city-loc-8) 1 ) 
    (= (at package-5 city-loc-4) 1 ) 
    (= (at package-6 city-loc-15) 1 ) 
    (= (at package-3 city-loc-3) 1 ) 
    (= (at truck-2 city-loc-8) 1 ) 
    (= (in package-6 truck-1) 1 ) 
    (= (in package-2 truck-1) 1 ) 
    (= (capacity truck-1 capacity-2) 1 ) 
    (= (at truck-2 city-loc-6) 1 ) 
 )
 (:bound 332)
 (:use-cost-metric)
 )
 
 


 