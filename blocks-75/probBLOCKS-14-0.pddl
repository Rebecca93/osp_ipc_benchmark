(define (problem BLOCKS-14-0)
(:domain BLOCKS)
(:objects I D B L C K M H J N E F G A )
(:INIT (CLEAR A) (CLEAR G) (CLEAR F) (ONTABLE E) (ONTABLE N) (ONTABLE F)
 (ON A J) (ON J H) (ON H M) (ON M K) (ON K C) (ON C L) (ON L B) (ON B E)
 (ON G D) (ON D I) (ON I N) (HANDEMPTY))
 (:utility 
    (= (ON E L) 1 ) 
    (= (ON L F) 1 ) 
    (= (ON F B) 1 ) 
    (= (ON B J) 1 ) 
    (= (ON J I) 1 ) 
    (= (ON I N) 1 ) 
    (= (ON N C) 1 ) 
    (= (ON C K) 1 ) 
    (= (ON K G) 1 ) 
    (= (ON G D) 1 ) 
    (= (ON D M) 1 ) 
    (= (ON M A) 1 ) 
    (= (ON A H) 1 ) 
    (= (on m i) 1 ) 
    (= (on i j) 1 ) 
    (= (ontable f) 1 ) 
    (= (holding g) 1 ) 
    (= (on m n) 1 ) 
    (= (on i k) 1 ) 
    (= (on b a) 1 ) 
    (= (on k a) 1 ) 
    (= (on e h) 1 ) 
    (= (holding k) 1 ) 
    (= (on a f) 1 ) 
 )
 (:bound 28)
 )
 


