(define (problem BLOCKS-6-1)
(:domain BLOCKS)
(:objects F D C E B A )
(:INIT (CLEAR A) (CLEAR B) (CLEAR E) (CLEAR C) (CLEAR D) (ONTABLE F)
 (ONTABLE B) (ONTABLE E) (ONTABLE C) (ONTABLE D) (ON A F) (HANDEMPTY))
 (:utility 
    (= (ON E F) 1 ) 
    (= (ON F C) 1 ) 
    (= (ON C B) 1 ) 
    (= (ON B A) 1 ) 
    (= (ON A D) 1 ) 
    (= (on b a) 1 ) 
    (= (on f a) 1 ) 
 )
 (:bound 7)
 )
 


