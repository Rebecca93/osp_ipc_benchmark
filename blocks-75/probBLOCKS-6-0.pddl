(define (problem BLOCKS-6-0)
(:domain BLOCKS)
(:objects E A B C F D )
(:INIT (CLEAR D) (CLEAR F) (ONTABLE C) (ONTABLE B) (ON D A) (ON A C) (ON F E)
 (ON E B) (HANDEMPTY))
 (:utility 
    (= (ON C B) 1 ) 
    (= (ON B A) 1 ) 
    (= (ON A E) 1 ) 
    (= (ON E F) 1 ) 
    (= (ON F D) 1 ) 
    (= (on a e) 1 ) 
    (= (on f a) 1 ) 
 )
 (:bound 9)
 )
 


