(define (problem BLOCKS-17-0)
(:domain BLOCKS)
(:objects C D E F B I J A N O K M P H G L Q )
(:INIT (CLEAR Q) (CLEAR L) (CLEAR G) (CLEAR H) (CLEAR P) (ONTABLE M)
 (ONTABLE K) (ONTABLE O) (ONTABLE N) (ONTABLE P) (ON Q A) (ON A J) (ON J I)
 (ON I B) (ON B M) (ON L F) (ON F E) (ON E K) (ON G D) (ON D C) (ON C O)
 (ON H N) (HANDEMPTY))
 (:utility 
    (= (ON Q N) 1 ) 
    (= (ON N L) 1 ) 
    (= (ON L O) 1 ) 
    (= (ON O J) 1 ) 
    (= (ON J H) 1 ) 
    (= (ON H C) 1 ) 
    (= (ON C E) 1 ) 
    (= (ON E M) 1 ) 
    (= (ON M P) 1 ) 
    (= (ON P A) 1 ) 
    (= (ON A G) 1 ) 
    (= (ON G B) 1 ) 
    (= (ON B I) 1 ) 
    (= (ON I K) 1 ) 
    (= (ON K F) 1 ) 
    (= (ON F D) 1 ) 
    (= (on o l) 1 ) 
    (= (clear n) 1 ) 
    (= (on h o) 1 ) 
    (= (clear m) 1 ) 
    (= (on p h) 1 ) 
    (= (ontable k) 1 ) 
    (= (on g k) 1 ) 
    (= (on k m) 1 ) 
    (= (on b j) 1 ) 
    (= (holding p) 1 ) 
    (= (on b n) 1 ) 
    (= (on e j) 1 ) 
    (= (clear q) 1 ) 
    (= (clear j) 1 ) 
    (= (holding k) 1 ) 
    (= (on l n) 1 ) 
    (= (on i o) 1 ) 
 )
 (:bound 34)
 )
 


