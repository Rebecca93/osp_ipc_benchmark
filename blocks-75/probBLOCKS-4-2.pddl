(define (problem BLOCKS-4-2)
(:domain BLOCKS)
(:objects B D C A )
(:INIT (CLEAR A) (CLEAR C) (CLEAR D) (ONTABLE A) (ONTABLE B) (ONTABLE D)
 (ON C B) (HANDEMPTY))
 (:utility 
    (= (ON A B) 1 ) 
    (= (ON B C) 1 ) 
    (= (ON C D) 1 ) 
    (= (clear b) 1 ) 
 )
 (:bound 4)
 )
 


