(define (problem BLOCKS-6-2)
(:domain BLOCKS)
(:objects E F B D C A )
(:INIT (CLEAR A) (ONTABLE C) (ON A D) (ON D B) (ON B F) (ON F E) (ON E C)
 (HANDEMPTY))
 (:utility 
    (= (ON E F) 1 ) 
    (= (ON F A) 1 ) 
    (= (ON A B) 1 ) 
    (= (ON B C) 1 ) 
    (= (ON C D) 1 ) 
    (= (holding a) 1 ) 
    (= (clear f) 1 ) 
 )
 (:bound 15)
 )
 


