(define (problem BLOCKS-5-1)
(:domain BLOCKS)
(:objects A D C E B )
(:INIT (CLEAR B) (CLEAR E) (CLEAR C) (ONTABLE D) (ONTABLE E) (ONTABLE C)
 (ON B A) (ON A D) (HANDEMPTY))
 (:utility 
    (= (ON D C) 1 ) 
    (= (ON C B) 1 ) 
    (= (ON B A) 1 ) 
    (= (ON A E) 1 ) 
    (= (clear a) 1 ) 
    (= (clear c) 1 ) 
 )
 (:bound 7)
 )
 


