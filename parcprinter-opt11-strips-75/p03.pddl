(define (problem PrintJob)
(:domain eTipp)
(:objects
		dummy-sheet
		sheet1
		sheet2 - sheet_t
		image-1
		image-2 - image_t
)
(:init
		(Uninitialized)
		(= (total-cost) 1 )
		(Oppositeside Front Back)
		(Oppositeside Back Front)
		(Location dummy-sheet Some_Finisher_Tray)
		(Prevsheet sheet1 dummy-sheet)
		(Prevsheet sheet2 sheet1)
		(Sheetsize sheet1 Letter)
		(Sheetsize sheet2 Letter)
		(Location sheet1 Some_Feeder_Tray)
		(Location sheet2 Some_Feeder_Tray)
		(Imagecolor image-1 Color)
		(Imagecolor image-2 Black)
		(Notprintedwith sheet1 Front Black)
		(Notprintedwith sheet1 Back Black)
		(Notprintedwith sheet1 Front Color)
		(Notprintedwith sheet1 Back Color)
		(Notprintedwith sheet2 Front Black)
		(Notprintedwith sheet2 Back Black)
		(Notprintedwith sheet2 Front Color)
		(Notprintedwith sheet2 Back Color)
)
 (:utility 
    (= (Hasimage sheet1 Front image-1) 1 ) 
    (= (Notprintedwith sheet1 Front Black) 1 ) 
    (= (Notprintedwith sheet1 Back Black) 1 ) 
    (= (Notprintedwith sheet1 Back Color) 1 ) 
    (= (Hasimage sheet2 Front image-2) 1 ) 
    (= (Notprintedwith sheet2 Front Color) 1 ) 
    (= (Notprintedwith sheet2 Back Black) 1 ) 
    (= (Notprintedwith sheet2 Back Color) 1 ) 
    (= (Sideup sheet1 Front) 1 ) 
    (= (Sideup sheet2 Front) 1 ) 
    (= (Stackedin sheet1 sys_OutputTray) 1 ) 
    (= (Stackedin sheet2 sys_OutputTray) 1 ) 
    (= (location sheet2 om_bottomentry-lc2_exit) 1 ) 
    (= (notprintedwith sheet1 front color) 1 ) 
    (= (location sheet2 im1_bottomexit-lc1_entry) 1 ) 
    (= (available ube-rsrc) 1 ) 
 )
 (:bound 382692)
 (:use-cost-metric)
 )
 
 


