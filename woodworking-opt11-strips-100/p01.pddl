; woodworking task with 5 parts and 100% wood
; Machines:
;   1 grinder
;   1 glazer
;   1 immersion-varnisher
;   1 planer
;   1 highspeed-saw
;   1 spray-varnisher
;   1 saw
; random seed: 370348

(define (problem wood-prob)
  (:domain woodworking)
  (:objects
    grinder0 - grinder
    glazer0 - glazer
    immersion-varnisher0 - immersion-varnisher
    planer0 - planer
    highspeed-saw0 - highspeed-saw
    spray-varnisher0 - spray-varnisher
    saw0 - saw
    green white red black - acolour
    teak mahogany - awood
    p0 p1 p2 p3 p4 - part
    b0 b1 - board
    s0 s1 s2 s3 s4 s5 - aboardsize
  )
  (:init
    (grind-treatment-change varnished colourfragments)
    (grind-treatment-change glazed untreated)
    (grind-treatment-change untreated untreated)
    (grind-treatment-change colourfragments untreated)
    (is-smooth smooth)
    (is-smooth verysmooth)
    (= (total-cost) 1 )
    (boardsize-successor s0 s1)
    (boardsize-successor s1 s2)
    (boardsize-successor s2 s3)
    (boardsize-successor s3 s4)
    (boardsize-successor s4 s5)
    (has-colour glazer0 green)
    (has-colour immersion-varnisher0 green)
    (empty highspeed-saw0)
    (has-colour spray-varnisher0 green)
    (unused p0)
    (goalsize p0 medium)
    (= (spray-varnish-cost p0) 1 )
    (= (glaze-cost p0) 1 )
    (= (grind-cost p0) 1 )
    (= (plane-cost p0) 1 )
    (unused p1)
    (goalsize p1 small)
    (= (spray-varnish-cost p1) 1 )
    (= (glaze-cost p1) 1 )
    (= (grind-cost p1) 1 )
    (= (plane-cost p1) 1 )
    (available p2)
    (colour p2 black)
    (wood p2 mahogany)
    (surface-condition p2 rough)
    (treatment p2 colourfragments)
    (goalsize p2 small)
    (= (spray-varnish-cost p2) 1 )
    (= (glaze-cost p2) 1 )
    (= (grind-cost p2) 1 )
    (= (plane-cost p2) 1 )
    (unused p3)
    (goalsize p3 large)
    (= (spray-varnish-cost p3) 1 )
    (= (glaze-cost p3) 1 )
    (= (grind-cost p3) 1 )
    (= (plane-cost p3) 1 )
    (unused p4)
    (goalsize p4 medium)
    (= (spray-varnish-cost p4) 1 )
    (= (glaze-cost p4) 1 )
    (= (grind-cost p4) 1 )
    (= (plane-cost p4) 1 )
    (boardsize b0 s3)
    (wood b0 teak)
    (surface-condition b0 rough)
    (available b0)
    (boardsize b1 s5)
    (wood b1 mahogany)
    (surface-condition b1 rough)
    (available b1)
  )
 (:utility 
    (= (available p0) 1 ) 
    (= (surface-condition p0 smooth) 1 ) 
    (= (treatment p0 varnished) 1 ) 
    (= (available p1) 1 ) 
    (= (wood p1 teak) 1 ) 
    (= (surface-condition p1 smooth) 1 ) 
    (= (treatment p1 varnished) 1 ) 
    (= (available p2) 1 ) 
    (= (colour p2 green) 1 ) 
    (= (surface-condition p2 smooth) 1 ) 
    (= (available p3) 1 ) 
    (= (wood p3 mahogany) 1 ) 
    (= (surface-condition p3 smooth) 1 ) 
    (= (available p4) 1 ) 
    (= (wood p4 teak) 1 ) 
    (= (treatment p4 glazed) 1 ) 
    (= (empty highspeed-saw0) 1 ) 
    (= (colour p1 natural) 1 ) 
    (= (colour p2 natural) 1 ) 
 )
 (:bound 195)
 (:use-cost-metric)
 )
 
 


  