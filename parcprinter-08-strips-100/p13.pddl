(define (problem PrintJob)
(:domain eTipp)
(:objects
		dummy-sheet
		sheet1
		sheet2
		sheet3 - sheet_t
		image-1
		image-2
		image-3 - image_t
)
(:init
		(Uninitialized)
		(= (total-cost) 1 )
		(Oppositeside Front Back)
		(Oppositeside Back Front)
		(Location dummy-sheet Some_Finisher_Tray)
		(Prevsheet sheet1 dummy-sheet)
		(Prevsheet sheet2 sheet1)
		(Prevsheet sheet3 sheet2)
		(Sheetsize sheet1 Letter)
		(Sheetsize sheet2 Letter)
		(Sheetsize sheet3 Letter)
		(Location sheet1 Some_Feeder_Tray)
		(Location sheet2 Some_Feeder_Tray)
		(Location sheet3 Some_Feeder_Tray)
		(Imagecolor image-1 Color)
		(Imagecolor image-2 Black)
		(Imagecolor image-3 Black)
		(Notprintedwith sheet1 Front Black)
		(Notprintedwith sheet1 Back Black)
		(Notprintedwith sheet1 Front Color)
		(Notprintedwith sheet1 Back Color)
		(Notprintedwith sheet2 Front Black)
		(Notprintedwith sheet2 Back Black)
		(Notprintedwith sheet2 Front Color)
		(Notprintedwith sheet2 Back Color)
		(Notprintedwith sheet3 Front Black)
		(Notprintedwith sheet3 Back Black)
		(Notprintedwith sheet3 Front Color)
		(Notprintedwith sheet3 Back Color)
)
 (:utility 
    (= (Hasimage sheet1 Front image-1) 1 ) 
    (= (Notprintedwith sheet1 Front Black) 1 ) 
    (= (Notprintedwith sheet1 Back Black) 1 ) 
    (= (Notprintedwith sheet1 Back Color) 1 ) 
    (= (Hasimage sheet2 Front image-2) 1 ) 
    (= (Notprintedwith sheet2 Front Color) 1 ) 
    (= (Notprintedwith sheet2 Back Black) 1 ) 
    (= (Notprintedwith sheet2 Back Color) 1 ) 
    (= (Hasimage sheet3 Front image-3) 1 ) 
    (= (Notprintedwith sheet3 Front Color) 1 ) 
    (= (Notprintedwith sheet3 Back Black) 1 ) 
    (= (Notprintedwith sheet3 Back Color) 1 ) 
    (= (Sideup sheet1 Front) 1 ) 
    (= (Sideup sheet2 Front) 1 ) 
    (= (Sideup sheet3 Front) 1 ) 
    (= (Stackedin sheet1 sys_OutputTray) 1 ) 
    (= (Stackedin sheet2 sys_OutputTray) 1 ) 
    (= (Stackedin sheet3 sys_OutputTray) 1 ) 
    (= (uninitialized) 1 ) 
    (= (location sheet2 rh1_entry-rh2_exit) 1 ) 
    (= (hasimage sheet2 back image-2) 1 ) 
    (= (location sheet2 uc2_exit-om_topentry) 1 ) 
    (= (location sheet3 ure_entry-uc2_offramp) 1 ) 
 )
 (:bound 693064)
 (:use-cost-metric)
 )
 
 


