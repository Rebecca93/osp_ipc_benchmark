(define (problem PrintJob)
(:domain eTipp)
(:objects
		dummy-sheet
		sheet1
		sheet2
		sheet3
		sheet4
		sheet5
		sheet6 - sheet_t
		image-1
		image-2
		image-3
		image-4
		image-5
		image-6 - image_t
)
(:init
		(Uninitialized)
		(= (total-cost) 1 )
		(Oppositeside Front Back)
		(Oppositeside Back Front)
		(Location dummy-sheet Some_Finisher_Tray)
		(Prevsheet sheet1 dummy-sheet)
		(Prevsheet sheet2 sheet1)
		(Prevsheet sheet3 sheet2)
		(Prevsheet sheet4 sheet3)
		(Prevsheet sheet5 sheet4)
		(Prevsheet sheet6 sheet5)
		(Sheetsize sheet1 Letter)
		(Sheetsize sheet2 Letter)
		(Sheetsize sheet3 Letter)
		(Sheetsize sheet4 Letter)
		(Sheetsize sheet5 Letter)
		(Sheetsize sheet6 Letter)
		(Location sheet1 Some_Feeder_Tray)
		(Location sheet2 Some_Feeder_Tray)
		(Location sheet3 Some_Feeder_Tray)
		(Location sheet4 Some_Feeder_Tray)
		(Location sheet5 Some_Feeder_Tray)
		(Location sheet6 Some_Feeder_Tray)
		(Imagecolor image-1 Black)
		(Imagecolor image-2 Color)
		(Imagecolor image-3 Color)
		(Imagecolor image-4 Color)
		(Imagecolor image-5 Color)
		(Imagecolor image-6 Black)
		(Notprintedwith sheet1 Front Black)
		(Notprintedwith sheet1 Back Black)
		(Notprintedwith sheet1 Front Color)
		(Notprintedwith sheet1 Back Color)
		(Notprintedwith sheet2 Front Black)
		(Notprintedwith sheet2 Back Black)
		(Notprintedwith sheet2 Front Color)
		(Notprintedwith sheet2 Back Color)
		(Notprintedwith sheet3 Front Black)
		(Notprintedwith sheet3 Back Black)
		(Notprintedwith sheet3 Front Color)
		(Notprintedwith sheet3 Back Color)
		(Notprintedwith sheet4 Front Black)
		(Notprintedwith sheet4 Back Black)
		(Notprintedwith sheet4 Front Color)
		(Notprintedwith sheet4 Back Color)
		(Notprintedwith sheet5 Front Black)
		(Notprintedwith sheet5 Back Black)
		(Notprintedwith sheet5 Front Color)
		(Notprintedwith sheet5 Back Color)
		(Notprintedwith sheet6 Front Black)
		(Notprintedwith sheet6 Back Black)
		(Notprintedwith sheet6 Front Color)
		(Notprintedwith sheet6 Back Color)
)
 (:utility 
    (= (Hasimage sheet1 Front image-1) 1 ) 
    (= (Notprintedwith sheet1 Front Color) 1 ) 
    (= (Notprintedwith sheet1 Back Black) 1 ) 
    (= (Notprintedwith sheet1 Back Color) 1 ) 
    (= (Hasimage sheet2 Front image-2) 1 ) 
    (= (Notprintedwith sheet2 Front Black) 1 ) 
    (= (Notprintedwith sheet2 Back Black) 1 ) 
    (= (Notprintedwith sheet2 Back Color) 1 ) 
    (= (Hasimage sheet3 Front image-3) 1 ) 
    (= (Notprintedwith sheet3 Front Black) 1 ) 
    (= (Notprintedwith sheet3 Back Black) 1 ) 
    (= (Notprintedwith sheet3 Back Color) 1 ) 
    (= (Hasimage sheet4 Front image-4) 1 ) 
    (= (Notprintedwith sheet4 Front Black) 1 ) 
    (= (Notprintedwith sheet4 Back Black) 1 ) 
    (= (Notprintedwith sheet4 Back Color) 1 ) 
    (= (Hasimage sheet5 Front image-5) 1 ) 
    (= (Notprintedwith sheet5 Front Black) 1 ) 
    (= (Notprintedwith sheet5 Back Black) 1 ) 
    (= (Notprintedwith sheet5 Back Color) 1 ) 
    (= (Hasimage sheet6 Front image-6) 1 ) 
    (= (Notprintedwith sheet6 Front Color) 1 ) 
    (= (Notprintedwith sheet6 Back Black) 1 ) 
    (= (Notprintedwith sheet6 Back Color) 1 ) 
    (= (Sideup sheet1 Front) 1 ) 
    (= (Sideup sheet2 Front) 1 ) 
    (= (Sideup sheet3 Front) 1 ) 
    (= (Sideup sheet4 Front) 1 ) 
    (= (Sideup sheet5 Front) 1 ) 
    (= (Sideup sheet6 Front) 1 ) 
    (= (Stackedin sheet1 sys_OutputTray) 1 ) 
    (= (Stackedin sheet2 sys_OutputTray) 1 ) 
    (= (Stackedin sheet3 sys_OutputTray) 1 ) 
    (= (Stackedin sheet4 sys_OutputTray) 1 ) 
    (= (Stackedin sheet5 sys_OutputTray) 1 ) 
    (= (Stackedin sheet6 sys_OutputTray) 1 ) 
    (= (notprintedwith sheet5 front color) 1 ) 
    (= (location sheet1 ube_entry-uc1_offramp) 1 ) 
    (= (location sheet6 ube_entry-uc1_offramp) 1 ) 
    (= (location sheet2 uc2_exit-om_topentry) 1 ) 
    (= (location sheet1 rh1_entry-rh2_exit) 1 ) 
    (= (stackedin sheet4 sys_outputtray) 1 ) 
    (= (location sheet3 ure_entry-uc2_offramp) 1 ) 
    (= (sideup sheet3 back) 1 ) 
    (= (hasimage sheet6 front image-4) 1 ) 
    (= (location sheet5 ure_entry-uc2_offramp) 1 ) 
    (= (location sheet3 uc1_entry-im1_topexit) 1 ) 
    (= (location sheet2 om_returnexit-rh2_entry) 1 ) 
 )
 (:bound 1675408)
 (:use-cost-metric)
 )
 
 


