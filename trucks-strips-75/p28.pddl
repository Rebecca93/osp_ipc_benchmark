; Time: 0.36 seconds
(define (problem GROUNDED-TRUCK-28)
(:domain GROUNDED-TRUCKS)
(:init
(FOO)
(time-now_t0)
(at_package18_l2)
(at_package17_l2)
(at_package16_l2)
(at_package15_l2)
(at_package14_l2)
(at_package13_l2)
(at_package12_l6)
(at_package11_l6)
(at_package10_l6)
(at_package9_l6)
(at_package8_l6)
(at_package7_l6)
(at_package6_l1)
(at_package5_l1)
(at_package4_l1)
(at_package3_l1)
(at_package2_l1)
(at_package1_l1)
(free_a6_truck1)
(free_a5_truck1)
(free_a4_truck1)
(free_a3_truck1)
(free_a2_truck1)
(free_a1_truck1)
(at_truck1_l7)
)
 (:utility 
    (= (delivered_package18_l5_t21) 1 ) 
    (= (at-destination_package17_l5) 1 ) 
    (= (at-destination_package16_l3) 1 ) 
    (= (delivered_package15_l1_t21) 1 ) 
    (= (at-destination_package14_l3) 1 ) 
    (= (delivered_package13_l6_t21) 1 ) 
    (= (delivered_package12_l2_t14) 1 ) 
    (= (delivered_package11_l1_t14) 1 ) 
    (= (at-destination_package10_l5) 1 ) 
    (= (at-destination_package9_l4) 1 ) 
    (= (delivered_package8_l4_t14) 1 ) 
    (= (delivered_package7_l3_t14) 1 ) 
    (= (at-destination_package6_l2) 1 ) 
    (= (delivered_package5_l2_t7) 1 ) 
    (= (delivered_package4_l2_t7) 1 ) 
    (= (delivered_package3_l3_t7) 1 ) 
    (= (delivered_package2_l5_t7) 1 ) 
    (= (delivered_package1_l3_t7) 1 ) 
    (= (delivered_package3_l2_t15) 1 ) 
    (= (delivered_package12_l2_t19) 1 ) 
    (= (at_package10_l2) 1 ) 
    (= (delivered_package11_l4_t18) 1 ) 
    (= (delivered_package15_l1_t13) 1 ) 
    (= (delivered_package3_l3_t9) 1 ) 
    (= (delivered_package10_l2_t18) 1 ) 
    (= (delivered_package7_l4_t12) 1 ) 
    (= (delivered_package6_l4_t11) 1 ) 
    (= (delivered_package8_l2_t9) 1 ) 
    (= (delivered_package8_l4_t16) 1 ) 
    (= (at_package10_l4) 1 ) 
    (= (delivered_package4_l6_t14) 1 ) 
    (= (delivered_package9_l7_t18) 1 ) 
    (= (at_package5_l2) 1 ) 
    (= (in_package14_truck1_a4) 1 ) 
    (= (at_package17_l4) 1 ) 
    (= (delivered_package15_l7_t21) 1 ) 
    (= (delivered_package4_l6_t4) 1 ) 
    (= (delivered_package13_l2_t2) 1 ) 
    (= (delivered_package5_l1_t9) 1 ) 
    (= (delivered_package10_l6_t12) 1 ) 
    (= (delivered_package18_l2_t14) 1 ) 
    (= (delivered_package2_l3_t2) 1 ) 
    (= (delivered_package10_l7_t13) 1 ) 
    (= (delivered_package11_l1_t9) 1 ) 
    (= (delivered_package16_l5_t3) 1 ) 
    (= (delivered_package16_l3_t12) 1 ) 
    (= (delivered_package18_l1_t4) 1 ) 
    (= (delivered_package12_l7_t19) 1 ) 
    (= (delivered_package1_l2_t6) 1 ) 
    (= (time-now_t17) 1 ) 
    (= (time-now_t2) 1 ) 
    (= (delivered_package10_l7_t7) 1 ) 
    (= (delivered_package16_l4_t18) 1 ) 
    (= (delivered_package2_l7_t11) 1 ) 
    (= (delivered_package12_l2_t11) 1 ) 
    (= (delivered_package5_l2_t18) 1 ) 
    (= (delivered_package16_l5_t2) 1 ) 
    (= (delivered_package7_l4_t8) 1 ) 
    (= (at-destination_package11_l1) 1 ) 
    (= (delivered_package17_l5_t10) 1 ) 
    (= (delivered_package18_l5_t1) 1 ) 
    (= (delivered_package7_l5_t17) 1 ) 
    (= (at-destination_package16_l7) 1 ) 
    (= (delivered_package12_l6_t15) 1 ) 
    (= (delivered_package2_l6_t10) 1 ) 
    (= (delivered_package1_l2_t7) 1 ) 
    (= (delivered_package10_l2_t1) 1 ) 
    (= (delivered_package8_l5_t17) 1 ) 
    (= (delivered_package17_l7_t3) 1 ) 
    (= (delivered_package15_l5_t9) 1 ) 
    (= (in_package13_truck1_a6) 1 ) 
    (= (time-now_t1) 1 ) 
    (= (delivered_package3_l7_t21) 1 ) 
    (= (delivered_package13_l6_t10) 1 ) 
    (= (delivered_package2_l5_t19) 1 ) 
    (= (delivered_package14_l6_t15) 1 ) 
    (= (delivered_package11_l4_t6) 1 ) 
    (= (delivered_package9_l6_t13) 1 ) 
    (= (time-now_t16) 1 ) 
    (= (delivered_package16_l6_t19) 1 ) 
    (= (in_package12_truck1_a3) 1 ) 
    (= (delivered_package3_l6_t14) 1 ) 
    (= (at_package14_l4) 1 ) 
    (= (delivered_package2_l7_t13) 1 ) 
    (= (at_package16_l2) 1 ) 
    (= (delivered_package10_l5_t5) 1 ) 
    (= (delivered_package14_l7_t3) 1 ) 
    (= (delivered_package13_l1_t12) 1 ) 
    (= (in_package18_truck1_a4) 1 ) 
    (= (at-destination_package2_l1) 1 ) 
    (= (delivered_package1_l7_t12) 1 ) 
    (= (delivered_package4_l3_t5) 1 ) 
    (= (delivered_package9_l6_t18) 1 ) 
    (= (delivered_package1_l2_t17) 1 ) 
    (= (delivered_package6_l3_t1) 1 ) 
    (= (delivered_package4_l2_t13) 1 ) 
    (= (delivered_package16_l6_t18) 1 ) 
    (= (delivered_package16_l6_t14) 1 ) 
    (= (delivered_package13_l1_t19) 1 ) 
    (= (delivered_package10_l1_t12) 1 ) 
    (= (delivered_package18_l3_t8) 1 ) 
    (= (delivered_package11_l6_t2) 1 ) 
    (= (at_package10_l6) 1 ) 
    (= (delivered_package4_l2_t6) 1 ) 
    (= (delivered_package7_l4_t1) 1 ) 
    (= (delivered_package14_l5_t21) 1 ) 
    (= (delivered_package16_l3_t11) 1 ) 
    (= (in_package11_truck1_a5) 1 ) 
    (= (delivered_package2_l5_t1) 1 ) 
    (= (delivered_package4_l1_t21) 1 ) 
    (= (delivered_package4_l1_t19) 1 ) 
    (= (delivered_package17_l1_t15) 1 ) 
    (= (delivered_package9_l7_t12) 1 ) 
    (= (delivered_package6_l1_t21) 1 ) 
    (= (in_package13_truck1_a4) 1 ) 
    (= (delivered_package2_l4_t13) 1 ) 
    (= (delivered_package16_l7_t2) 1 ) 
    (= (at-destination_package15_l7) 1 ) 
    (= (delivered_package15_l2_t1) 1 ) 
    (= (delivered_package2_l2_t18) 1 ) 
    (= (at_package10_l7) 1 ) 
    (= (delivered_package11_l7_t20) 1 ) 
    (= (delivered_package12_l3_t11) 1 ) 
    (= (delivered_package15_l6_t21) 1 ) 
    (= (at_package3_l2) 1 ) 
    (= (delivered_package10_l5_t3) 1 ) 
    (= (delivered_package3_l7_t3) 1 ) 
    (= (delivered_package9_l5_t3) 1 ) 
    (= (delivered_package13_l5_t2) 1 ) 
    (= (delivered_package9_l1_t3) 1 ) 
    (= (delivered_package4_l3_t19) 1 ) 
    (= (delivered_package11_l5_t1) 1 ) 
    (= (delivered_package12_l2_t20) 1 ) 
    (= (delivered_package15_l6_t1) 1 ) 
    (= (delivered_package1_l2_t4) 1 ) 
    (= (delivered_package11_l1_t12) 1 ) 
    (= (delivered_package12_l3_t18) 1 ) 
    (= (delivered_package10_l2_t6) 1 ) 
    (= (delivered_package2_l4_t1) 1 ) 
    (= (delivered_package7_l4_t6) 1 ) 
    (= (delivered_package9_l7_t16) 1 ) 
    (= (delivered_package15_l1_t20) 1 ) 
    (= (at-destination_package3_l4) 1 ) 
    (= (delivered_package11_l3_t15) 1 ) 
    (= (delivered_package12_l4_t19) 1 ) 
    (= (delivered_package6_l4_t7) 1 ) 
    (= (delivered_package17_l3_t12) 1 ) 
    (= (delivered_package18_l1_t7) 1 ) 
    (= (delivered_package9_l3_t6) 1 ) 
    (= (time-now_t12) 1 ) 
    (= (delivered_package5_l4_t12) 1 ) 
    (= (delivered_package14_l5_t19) 1 ) 
    (= (delivered_package9_l7_t8) 1 ) 
    (= (delivered_package9_l3_t20) 1 ) 
    (= (delivered_package16_l2_t19) 1 ) 
    (= (delivered_package7_l5_t8) 1 ) 
    (= (delivered_package10_l1_t19) 1 ) 
    (= (delivered_package15_l1_t1) 1 ) 
    (= (delivered_package17_l7_t4) 1 ) 
    (= (delivered_package5_l2_t14) 1 ) 
    (= (delivered_package5_l7_t5) 1 ) 
    (= (delivered_package4_l6_t17) 1 ) 
    (= (delivered_package11_l7_t10) 1 ) 
    (= (time-now_t4) 1 ) 
    (= (delivered_package17_l1_t20) 1 ) 
    (= (delivered_package6_l2_t18) 1 ) 
    (= (delivered_package17_l3_t9) 1 ) 
    (= (delivered_package2_l7_t8) 1 ) 
    (= (delivered_package11_l5_t7) 1 ) 
 )
 (:bound 45)
 )
 


