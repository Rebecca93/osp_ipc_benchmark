; Time: 0.01 seconds
(define (problem GROUNDED-TRUCK-2)
(:domain GROUNDED-TRUCKS)
(:init
(FOO)
(time-now_t0)
(at_package4_l1)
(at_package3_l1)
(at_package2_l1)
(at_package1_l1)
(free_a2_truck1)
(free_a1_truck1)
(at_truck1_l2)
)
 (:utility 
    (= (delivered_package4_l3_t6) 1 ) 
    (= (at-destination_package3_l2) 1 ) 
    (= (delivered_package2_l2_t3) 1 ) 
    (= (delivered_package1_l2_t3) 1 ) 
    (= (delivered_package1_l1_t4) 1 ) 
    (= (at_package3_l2) 1 ) 
    (= (delivered_package1_l1_t2) 1 ) 
    (= (delivered_package1_l2_t2) 1 ) 
    (= (delivered_package4_l2_t3) 1 ) 
 )
 (:bound 17)
 )
 


