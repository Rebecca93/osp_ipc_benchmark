; woodworking task with 3 parts and 140% wood
; Machines:
;   1 grinder
;   1 glazer
;   1 immersion-varnisher
;   1 planer
;   1 highspeed-saw
;   1 spray-varnisher
;   1 saw
; random seed: 854322

(define (problem wood-prob)
  (:domain woodworking)
  (:objects
    grinder0 - grinder
    glazer0 - glazer
    immersion-varnisher0 - immersion-varnisher
    planer0 - planer
    highspeed-saw0 - highspeed-saw
    spray-varnisher0 - spray-varnisher
    saw0 - saw
    green mauve - acolour
    cherry beech - awood
    p0 p1 p2 - part
    b0 b1 - board
    s0 s1 s2 s3 - aboardsize
  )
  (:init
    (grind-treatment-change varnished colourfragments)
    (grind-treatment-change glazed untreated)
    (grind-treatment-change untreated untreated)
    (grind-treatment-change colourfragments untreated)
    (is-smooth smooth)
    (is-smooth verysmooth)
    (= (total-cost) 1 )
    (boardsize-successor s0 s1)
    (boardsize-successor s1 s2)
    (boardsize-successor s2 s3)
    (has-colour glazer0 mauve)
    (has-colour glazer0 green)
    (has-colour immersion-varnisher0 mauve)
    (empty highspeed-saw0)
    (has-colour spray-varnisher0 mauve)
    (unused p0)
    (goalsize p0 medium)
    (= (spray-varnish-cost p0) 1 )
    (= (glaze-cost p0) 1 )
    (= (grind-cost p0) 1 )
    (= (plane-cost p0) 1 )
    (unused p1)
    (goalsize p1 medium)
    (= (spray-varnish-cost p1) 1 )
    (= (glaze-cost p1) 1 )
    (= (grind-cost p1) 1 )
    (= (plane-cost p1) 1 )
    (available p2)
    (colour p2 natural)
    (wood p2 beech)
    (surface-condition p2 verysmooth)
    (treatment p2 colourfragments)
    (goalsize p2 small)
    (= (spray-varnish-cost p2) 1 )
    (= (glaze-cost p2) 1 )
    (= (grind-cost p2) 1 )
    (= (plane-cost p2) 1 )
    (boardsize b0 s3)
    (wood b0 beech)
    (surface-condition b0 rough)
    (available b0)
    (boardsize b1 s3)
    (wood b1 cherry)
    (surface-condition b1 rough)
    (available b1)
  )
 (:utility 
    (= (available p0) 1 ) 
    (= (colour p0 mauve) 1 ) 
    (= (wood p0 beech) 1 ) 
    (= (surface-condition p0 verysmooth) 1 ) 
    (= (treatment p0 varnished) 1 ) 
    (= (available p1) 1 ) 
    (= (colour p1 green) 1 ) 
    (= (wood p1 cherry) 1 ) 
    (= (surface-condition p1 smooth) 1 ) 
    (= (treatment p1 glazed) 1 ) 
    (= (available p2) 1 ) 
    (= (colour p2 mauve) 1 ) 
    (= (wood p2 beech) 1 ) 
    (= (available b1) 1 ) 
 )
 (:bound 170)
 (:use-cost-metric)
 )
 
 


  