(define (problem grounded-STRIPS-PSR-S8-N1-L4-F10)
(:domain grounded-STRIPS-PSR)
(:init
(do-normal)
(NOT-UPDATED-CB1)
(NOT-CLOSED-SD3)
(CLOSED-SD6)
(CLOSED-SD5)
(CLOSED-SD4)
(CLOSED-SD2)
(CLOSED-SD1)
(CLOSED-CB1)
)
 (:utility 
    (= (do-normal) 1 ) 
    (= (CLOSED-SD6) 1 ) 
    (= (CLOSED-CB1) 1 ) 
    (= (CLOSED-SD4) 1 ) 
    (= (CLOSED-SD2) 1 ) 
    (= (CLOSED-SD1) 1 ) 
    (= (UPDATED-CB1) 1 ) 
    (= (not-updated-cb1) 1 ) 
 )
 (:bound 2)
 )
 


