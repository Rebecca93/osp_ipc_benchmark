(define (problem grounded-STRIPS-PSR-S41-N3-L4-F30)
(:domain grounded-STRIPS-PSR)
(:init
(do-normal)
(NOT-UPDATED-CB1)
(NOT-UPDATED-CB2)
(NOT-UPDATED-CB3)
(NOT-CLOSED-SD5)
(NOT-CLOSED-SD9)
(CLOSED-SD12)
(CLOSED-SD11)
(CLOSED-SD10)
(CLOSED-SD8)
(CLOSED-SD7)
(CLOSED-SD6)
(CLOSED-SD4)
(CLOSED-SD3)
(CLOSED-SD2)
(CLOSED-SD1)
(CLOSED-CB3)
(CLOSED-CB2)
(CLOSED-CB1)
)
 (:utility 
    (= (do-normal) 1 ) 
    (= (CLOSED-SD10) 1 ) 
    (= (CLOSED-CB3) 1 ) 
    (= (CLOSED-CB1) 1 ) 
    (= (UPDATED-CB1) 1 ) 
    (= (UPDATED-CB2) 1 ) 
    (= (UPDATED-CB3) 1 ) 
    (= (closed-cb2) 1 ) 
    (= (do-close_sd11-condeffs) 1 ) 
 )
 (:bound 4)
 )
 


