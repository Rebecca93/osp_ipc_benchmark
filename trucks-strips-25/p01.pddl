; Time: 0.01 seconds
(define (problem GROUNDED-TRUCK-1)
(:domain GROUNDED-TRUCKS)
(:init
(FOO)
(time-now_t0)
(at_package3_l2)
(at_package2_l2)
(at_package1_l2)
(free_a2_truck1)
(free_a1_truck1)
(at_truck1_l3)
)
 (:utility 
    (= (delivered_package3_l1_t6) 1 ) 
    (= (at-destination_package2_l1) 1 ) 
    (= (delivered_package1_l3_t3) 1 ) 
    (= (in_package2_truck1_a1) 1 ) 
    (= (delivered_package1_l1_t2) 1 ) 
    (= (delivered_package2_l1_t2) 1 ) 
    (= (delivered_package2_l3_t5) 1 ) 
 )
 (:bound 3)
 )
 


