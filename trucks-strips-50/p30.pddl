; Time: 0.71 seconds
(define (problem GROUNDED-TRUCK-30)
(:domain GROUNDED-TRUCKS)
(:init
(FOO)
(time-now_t0)
(at_package20_l5)
(at_package19_l5)
(at_package18_l6)
(at_package17_l6)
(at_package16_l6)
(at_package15_l6)
(at_package14_l6)
(at_package13_l6)
(at_package12_l2)
(at_package11_l2)
(at_package10_l2)
(at_package9_l2)
(at_package8_l2)
(at_package7_l2)
(at_package6_l2)
(at_package5_l2)
(at_package4_l2)
(at_package3_l2)
(at_package2_l2)
(at_package1_l2)
(free_a6_truck1)
(free_a5_truck1)
(free_a4_truck1)
(free_a3_truck1)
(free_a2_truck1)
(free_a1_truck1)
(at_truck1_l7)
)
 (:utility 
    (= (delivered_package20_l4_t28) 1 ) 
    (= (delivered_package19_l7_t28) 1 ) 
    (= (delivered_package18_l4_t21) 1 ) 
    (= (delivered_package17_l4_t21) 1 ) 
    (= (delivered_package16_l3_t21) 1 ) 
    (= (at-destination_package15_l4) 1 ) 
    (= (delivered_package14_l5_t21) 1 ) 
    (= (delivered_package13_l4_t21) 1 ) 
    (= (delivered_package12_l1_t14) 1 ) 
    (= (delivered_package11_l3_t14) 1 ) 
    (= (delivered_package10_l5_t14) 1 ) 
    (= (delivered_package9_l3_t14) 1 ) 
    (= (delivered_package8_l6_t14) 1 ) 
    (= (delivered_package7_l5_t14) 1 ) 
    (= (delivered_package6_l1_t7) 1 ) 
    (= (delivered_package5_l7_t7) 1 ) 
    (= (delivered_package4_l4_t7) 1 ) 
    (= (delivered_package3_l6_t7) 1 ) 
    (= (delivered_package2_l3_t7) 1 ) 
    (= (delivered_package1_l6_t7) 1 ) 
    (= (delivered_package20_l5_t7) 1 ) 
    (= (delivered_package14_l4_t17) 1 ) 
    (= (delivered_package12_l4_t2) 1 ) 
    (= (delivered_package11_l2_t28) 1 ) 
    (= (delivered_package3_l6_t17) 1 ) 
    (= (delivered_package7_l2_t6) 1 ) 
    (= (delivered_package7_l3_t8) 1 ) 
    (= (delivered_package8_l3_t14) 1 ) 
    (= (at-destination_package15_l2) 1 ) 
    (= (delivered_package4_l7_t23) 1 ) 
    (= (in_package19_truck1_a5) 1 ) 
    (= (delivered_package15_l3_t11) 1 ) 
    (= (delivered_package14_l3_t4) 1 ) 
    (= (delivered_package9_l3_t27) 1 ) 
    (= (delivered_package10_l3_t28) 1 ) 
    (= (delivered_package17_l2_t6) 1 ) 
    (= (delivered_package9_l7_t18) 1 ) 
    (= (at-destination_package4_l1) 1 ) 
    (= (delivered_package18_l3_t4) 1 ) 
    (= (delivered_package2_l6_t1) 1 ) 
    (= (delivered_package9_l2_t19) 1 ) 
    (= (delivered_package17_l6_t14) 1 ) 
    (= (delivered_package17_l3_t14) 1 ) 
    (= (delivered_package10_l5_t18) 1 ) 
    (= (delivered_package8_l6_t2) 1 ) 
    (= (delivered_package12_l1_t4) 1 ) 
    (= (delivered_package13_l4_t22) 1 ) 
    (= (delivered_package12_l4_t5) 1 ) 
    (= (delivered_package5_l7_t27) 1 ) 
    (= (delivered_package5_l2_t21) 1 ) 
    (= (delivered_package7_l6_t10) 1 ) 
    (= (at-destination_package6_l5) 1 ) 
    (= (delivered_package11_l3_t5) 1 ) 
    (= (delivered_package19_l7_t18) 1 ) 
    (= (delivered_package13_l3_t11) 1 ) 
    (= (delivered_package3_l5_t13) 1 ) 
    (= (delivered_package12_l3_t1) 1 ) 
    (= (delivered_package11_l6_t9) 1 ) 
    (= (delivered_package7_l1_t9) 1 ) 
    (= (delivered_package9_l7_t20) 1 ) 
    (= (delivered_package1_l1_t14) 1 ) 
    (= (delivered_package20_l2_t24) 1 ) 
    (= (delivered_package4_l1_t26) 1 ) 
    (= (delivered_package19_l2_t2) 1 ) 
    (= (delivered_package14_l3_t7) 1 ) 
    (= (delivered_package6_l4_t16) 1 ) 
    (= (delivered_package12_l2_t7) 1 ) 
    (= (delivered_package13_l4_t19) 1 ) 
    (= (delivered_package2_l6_t8) 1 ) 
    (= (delivered_package18_l3_t28) 1 ) 
    (= (delivered_package16_l7_t26) 1 ) 
    (= (at_package3_l6) 1 ) 
    (= (delivered_package16_l7_t23) 1 ) 
    (= (delivered_package12_l6_t24) 1 ) 
    (= (delivered_package8_l2_t7) 1 ) 
    (= (delivered_package10_l3_t22) 1 ) 
    (= (delivered_package16_l7_t18) 1 ) 
    (= (in_package19_truck1_a3) 1 ) 
    (= (delivered_package7_l3_t10) 1 ) 
    (= (delivered_package10_l7_t14) 1 ) 
    (= (delivered_package5_l5_t7) 1 ) 
    (= (delivered_package10_l7_t27) 1 ) 
    (= (delivered_package18_l6_t8) 1 ) 
    (= (delivered_package19_l7_t1) 1 ) 
    (= (at_package4_l4) 1 ) 
    (= (delivered_package4_l1_t17) 1 ) 
    (= (delivered_package1_l1_t12) 1 ) 
    (= (delivered_package12_l1_t6) 1 ) 
    (= (delivered_package7_l1_t24) 1 ) 
    (= (delivered_package16_l6_t9) 1 ) 
    (= (delivered_package18_l1_t18) 1 ) 
    (= (in_package2_truck1_a1) 1 ) 
    (= (delivered_package11_l6_t14) 1 ) 
    (= (delivered_package5_l5_t8) 1 ) 
    (= (delivered_package19_l6_t11) 1 ) 
    (= (at-destination_package11_l1) 1 ) 
    (= (delivered_package20_l4_t23) 1 ) 
    (= (delivered_package9_l6_t13) 1 ) 
    (= (at-destination_package3_l3) 1 ) 
    (= (delivered_package10_l5_t27) 1 ) 
    (= (delivered_package3_l6_t8) 1 ) 
    (= (delivered_package10_l7_t26) 1 ) 
    (= (delivered_package3_l5_t26) 1 ) 
    (= (delivered_package2_l6_t21) 1 ) 
    (= (delivered_package12_l4_t9) 1 ) 
    (= (delivered_package20_l6_t5) 1 ) 
    (= (delivered_package6_l5_t17) 1 ) 
    (= (delivered_package6_l6_t14) 1 ) 
    (= (delivered_package8_l4_t9) 1 ) 
    (= (delivered_package1_l6_t10) 1 ) 
    (= (delivered_package18_l1_t13) 1 ) 
    (= (delivered_package3_l4_t18) 1 ) 
    (= (delivered_package15_l5_t16) 1 ) 
    (= (delivered_package16_l1_t24) 1 ) 
    (= (delivered_package11_l6_t25) 1 ) 
    (= (delivered_package1_l7_t12) 1 ) 
    (= (delivered_package6_l5_t18) 1 ) 
    (= (delivered_package18_l5_t2) 1 ) 
    (= (delivered_package6_l6_t1) 1 ) 
    (= (delivered_package5_l2_t10) 1 ) 
    (= (delivered_package11_l2_t4) 1 ) 
    (= (delivered_package13_l3_t26) 1 ) 
    (= (delivered_package3_l1_t2) 1 ) 
    (= (delivered_package15_l2_t4) 1 ) 
    (= (delivered_package8_l5_t17) 1 ) 
    (= (delivered_package20_l2_t3) 1 ) 
    (= (delivered_package8_l3_t18) 1 ) 
    (= (delivered_package11_l4_t1) 1 ) 
    (= (delivered_package12_l6_t1) 1 ) 
    (= (delivered_package2_l4_t22) 1 ) 
    (= (delivered_package16_l7_t21) 1 ) 
    (= (delivered_package20_l5_t27) 1 ) 
    (= (delivered_package20_l4_t10) 1 ) 
    (= (delivered_package12_l4_t17) 1 ) 
    (= (delivered_package20_l7_t20) 1 ) 
    (= (delivered_package14_l3_t11) 1 ) 
    (= (delivered_package20_l6_t2) 1 ) 
    (= (delivered_package13_l2_t2) 1 ) 
    (= (delivered_package6_l3_t26) 1 ) 
    (= (delivered_package13_l5_t1) 1 ) 
    (= (delivered_package5_l6_t6) 1 ) 
    (= (delivered_package2_l5_t1) 1 ) 
    (= (delivered_package11_l1_t14) 1 ) 
    (= (at-destination_package10_l5) 1 ) 
    (= (delivered_package8_l4_t23) 1 ) 
    (= (delivered_package19_l5_t11) 1 ) 
    (= (delivered_package7_l7_t14) 1 ) 
    (= (delivered_package13_l6_t28) 1 ) 
    (= (delivered_package16_l7_t2) 1 ) 
    (= (delivered_package20_l4_t27) 1 ) 
    (= (delivered_package2_l4_t24) 1 ) 
    (= (delivered_package10_l5_t12) 1 ) 
    (= (delivered_package17_l2_t24) 1 ) 
    (= (at-destination_package8_l4) 1 ) 
    (= (delivered_package19_l4_t21) 1 ) 
    (= (delivered_package2_l5_t17) 1 ) 
    (= (delivered_package12_l5_t16) 1 ) 
    (= (delivered_package12_l6_t2) 1 ) 
    (= (delivered_package9_l3_t15) 1 ) 
    (= (delivered_package8_l1_t16) 1 ) 
    (= (delivered_package13_l1_t10) 1 ) 
    (= (delivered_package4_l5_t21) 1 ) 
    (= (at-destination_package19_l3) 1 ) 
    (= (in_package3_truck1_a6) 1 ) 
    (= (delivered_package18_l2_t6) 1 ) 
    (= (at-destination_package11_l7) 1 ) 
    (= (delivered_package16_l6_t24) 1 ) 
    (= (delivered_package13_l2_t24) 1 ) 
    (= (delivered_package17_l1_t22) 1 ) 
    (= (delivered_package12_l1_t12) 1 ) 
    (= (delivered_package13_l4_t15) 1 ) 
    (= (delivered_package8_l2_t12) 1 ) 
    (= (delivered_package7_l4_t10) 1 ) 
    (= (delivered_package15_l5_t10) 1 ) 
    (= (delivered_package9_l2_t12) 1 ) 
    (= (delivered_package1_l3_t2) 1 ) 
    (= (delivered_package4_l3_t6) 1 ) 
    (= (delivered_package19_l2_t25) 1 ) 
    (= (delivered_package2_l2_t8) 1 ) 
    (= (delivered_package14_l2_t13) 1 ) 
    (= (delivered_package14_l3_t3) 1 ) 
    (= (delivered_package16_l2_t5) 1 ) 
    (= (delivered_package9_l1_t9) 1 ) 
    (= (delivered_package5_l7_t13) 1 ) 
    (= (delivered_package20_l2_t1) 1 ) 
    (= (in_package8_truck1_a6) 1 ) 
    (= (delivered_package3_l4_t11) 1 ) 
    (= (delivered_package5_l5_t17) 1 ) 
    (= (delivered_package4_l1_t13) 1 ) 
    (= (delivered_package2_l5_t16) 1 ) 
    (= (time-now_t4) 1 ) 
    (= (delivered_package20_l1_t18) 1 ) 
    (= (delivered_package16_l1_t2) 1 ) 
    (= (delivered_package16_l1_t21) 1 ) 
    (= (delivered_package20_l5_t21) 1 ) 
    (= (delivered_package5_l6_t10) 1 ) 
    (= (delivered_package1_l2_t3) 1 ) 
    (= (delivered_package10_l2_t6) 1 ) 
    (= (at_package15_l6) 1 ) 
    (= (delivered_package5_l5_t14) 1 ) 
    (= (delivered_package13_l2_t9) 1 ) 
    (= (delivered_package18_l7_t28) 1 ) 
    (= (delivered_package2_l2_t10) 1 ) 
    (= (delivered_package3_l1_t10) 1 ) 
    (= (delivered_package11_l6_t10) 1 ) 
    (= (delivered_package15_l2_t21) 1 ) 
    (= (delivered_package6_l1_t27) 1 ) 
    (= (at_package2_l3) 1 ) 
    (= (at-destination_package18_l7) 1 ) 
    (= (delivered_package9_l4_t16) 1 ) 
    (= (delivered_package13_l7_t15) 1 ) 
    (= (delivered_package9_l7_t17) 1 ) 
    (= (delivered_package11_l4_t14) 1 ) 
    (= (at_package4_l3) 1 ) 
    (= (delivered_package4_l5_t13) 1 ) 
    (= (time-now_t5) 1 ) 
    (= (at_package13_l3) 1 ) 
    (= (delivered_package16_l3_t20) 1 ) 
    (= (delivered_package6_l5_t13) 1 ) 
    (= (delivered_package10_l2_t5) 1 ) 
    (= (delivered_package12_l3_t3) 1 ) 
    (= (delivered_package14_l4_t4) 1 ) 
    (= (delivered_package13_l1_t17) 1 ) 
    (= (delivered_package11_l3_t3) 1 ) 
    (= (delivered_package19_l5_t23) 1 ) 
    (= (delivered_package12_l4_t10) 1 ) 
    (= (delivered_package11_l4_t21) 1 ) 
    (= (delivered_package17_l6_t16) 1 ) 
    (= (at-destination_package1_l6) 1 ) 
    (= (delivered_package12_l6_t26) 1 ) 
    (= (delivered_package6_l3_t19) 1 ) 
    (= (delivered_package12_l2_t13) 1 ) 
    (= (delivered_package15_l7_t9) 1 ) 
    (= (delivered_package14_l6_t26) 1 ) 
    (= (delivered_package2_l7_t8) 1 ) 
    (= (delivered_package6_l3_t21) 1 ) 
    (= (delivered_package8_l4_t24) 1 ) 
 )
 (:bound 33)
 )
 


