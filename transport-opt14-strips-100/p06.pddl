; Transport city-sequential-20nodes-1000size-4degree-100mindistance-3trucks-6packages-2014seed

(define (problem transport-city-sequential-20nodes-1000size-4degree-100mindistance-3trucks-6packages-2014seed)
 (:domain transport)
 (:objects
  city-loc-1 - location
  city-loc-2 - location
  city-loc-3 - location
  city-loc-4 - location
  city-loc-5 - location
  city-loc-6 - location
  city-loc-7 - location
  city-loc-8 - location
  city-loc-9 - location
  city-loc-10 - location
  city-loc-11 - location
  city-loc-12 - location
  city-loc-13 - location
  city-loc-14 - location
  city-loc-15 - location
  city-loc-16 - location
  city-loc-17 - location
  city-loc-18 - location
  city-loc-19 - location
  city-loc-20 - location
  truck-1 - vehicle
  truck-2 - vehicle
  truck-3 - vehicle
  package-1 - package
  package-2 - package
  package-3 - package
  package-4 - package
  package-5 - package
  package-6 - package
  capacity-0 - capacity-number
  capacity-1 - capacity-number
  capacity-2 - capacity-number
  capacity-3 - capacity-number
  capacity-4 - capacity-number
 )
 (:init
  (= (total-cost) 1 )
  (capacity-predecessor capacity-0 capacity-1)
  (capacity-predecessor capacity-1 capacity-2)
  (capacity-predecessor capacity-2 capacity-3)
  (capacity-predecessor capacity-3 capacity-4)
  ; 22,400 -> 315,398
  (road city-loc-4 city-loc-1)
  (= (road-length city-loc-4 city-loc-1) 1 )
  ; 315,398 -> 22,400
  (road city-loc-1 city-loc-4)
  (= (road-length city-loc-1 city-loc-4) 1 )
  ; 125,677 -> 427,691
  (road city-loc-5 city-loc-3)
  (= (road-length city-loc-5 city-loc-3) 1 )
  ; 427,691 -> 125,677
  (road city-loc-3 city-loc-5)
  (= (road-length city-loc-3 city-loc-5) 1 )
  ; 125,677 -> 22,400
  (road city-loc-5 city-loc-4)
  (= (road-length city-loc-5 city-loc-4) 1 )
  ; 22,400 -> 125,677
  (road city-loc-4 city-loc-5)
  (= (road-length city-loc-4 city-loc-5) 1 )
  ; 471,258 -> 315,398
  (road city-loc-7 city-loc-1)
  (= (road-length city-loc-7 city-loc-1) 1 )
  ; 315,398 -> 471,258
  (road city-loc-1 city-loc-7)
  (= (road-length city-loc-1 city-loc-7) 1 )
  ; 92,243 -> 315,398
  (road city-loc-8 city-loc-1)
  (= (road-length city-loc-8 city-loc-1) 1 )
  ; 315,398 -> 92,243
  (road city-loc-1 city-loc-8)
  (= (road-length city-loc-1 city-loc-8) 1 )
  ; 92,243 -> 22,400
  (road city-loc-8 city-loc-4)
  (= (road-length city-loc-8 city-loc-4) 1 )
  ; 22,400 -> 92,243
  (road city-loc-4 city-loc-8)
  (= (road-length city-loc-4 city-loc-8) 1 )
  ; 227,260 -> 315,398
  (road city-loc-9 city-loc-1)
  (= (road-length city-loc-9 city-loc-1) 1 )
  ; 315,398 -> 227,260
  (road city-loc-1 city-loc-9)
  (= (road-length city-loc-1 city-loc-9) 1 )
  ; 227,260 -> 22,400
  (road city-loc-9 city-loc-4)
  (= (road-length city-loc-9 city-loc-4) 1 )
  ; 22,400 -> 227,260
  (road city-loc-4 city-loc-9)
  (= (road-length city-loc-4 city-loc-9) 1 )
  ; 227,260 -> 471,258
  (road city-loc-9 city-loc-7)
  (= (road-length city-loc-9 city-loc-7) 1 )
  ; 471,258 -> 227,260
  (road city-loc-7 city-loc-9)
  (= (road-length city-loc-7 city-loc-9) 1 )
  ; 227,260 -> 92,243
  (road city-loc-9 city-loc-8)
  (= (road-length city-loc-9 city-loc-8) 1 )
  ; 92,243 -> 227,260
  (road city-loc-8 city-loc-9)
  (= (road-length city-loc-8 city-loc-9) 1 )
  ; 680,79 -> 471,258
  (road city-loc-10 city-loc-7)
  (= (road-length city-loc-10 city-loc-7) 1 )
  ; 471,258 -> 680,79
  (road city-loc-7 city-loc-10)
  (= (road-length city-loc-7 city-loc-10) 1 )
  ; 753,644 -> 973,444
  (road city-loc-11 city-loc-6)
  (= (road-length city-loc-11 city-loc-6) 1 )
  ; 973,444 -> 753,644
  (road city-loc-6 city-loc-11)
  (= (road-length city-loc-6 city-loc-11) 1 )
  ; 688,479 -> 973,444
  (road city-loc-12 city-loc-6)
  (= (road-length city-loc-12 city-loc-6) 1 )
  ; 973,444 -> 688,479
  (road city-loc-6 city-loc-12)
  (= (road-length city-loc-6 city-loc-12) 1 )
  ; 688,479 -> 753,644
  (road city-loc-12 city-loc-11)
  (= (road-length city-loc-12 city-loc-11) 1 )
  ; 753,644 -> 688,479
  (road city-loc-11 city-loc-12)
  (= (road-length city-loc-11 city-loc-12) 1 )
  ; 197,108 -> 92,243
  (road city-loc-13 city-loc-8)
  (= (road-length city-loc-13 city-loc-8) 1 )
  ; 92,243 -> 197,108
  (road city-loc-8 city-loc-13)
  (= (road-length city-loc-8 city-loc-13) 1 )
  ; 197,108 -> 227,260
  (road city-loc-13 city-loc-9)
  (= (road-length city-loc-13 city-loc-9) 1 )
  ; 227,260 -> 197,108
  (road city-loc-9 city-loc-13)
  (= (road-length city-loc-9 city-loc-13) 1 )
  ; 630,336 -> 471,258
  (road city-loc-14 city-loc-7)
  (= (road-length city-loc-14 city-loc-7) 1 )
  ; 471,258 -> 630,336
  (road city-loc-7 city-loc-14)
  (= (road-length city-loc-7 city-loc-14) 1 )
  ; 630,336 -> 680,79
  (road city-loc-14 city-loc-10)
  (= (road-length city-loc-14 city-loc-10) 1 )
  ; 680,79 -> 630,336
  (road city-loc-10 city-loc-14)
  (= (road-length city-loc-10 city-loc-14) 1 )
  ; 630,336 -> 688,479
  (road city-loc-14 city-loc-12)
  (= (road-length city-loc-14 city-loc-12) 1 )
  ; 688,479 -> 630,336
  (road city-loc-12 city-loc-14)
  (= (road-length city-loc-12 city-loc-14) 1 )
  ; 66,817 -> 125,677
  (road city-loc-15 city-loc-5)
  (= (road-length city-loc-15 city-loc-5) 1 )
  ; 125,677 -> 66,817
  (road city-loc-5 city-loc-15)
  (= (road-length city-loc-5 city-loc-15) 1 )
  ; 878,76 -> 680,79
  (road city-loc-16 city-loc-10)
  (= (road-length city-loc-16 city-loc-10) 1 )
  ; 680,79 -> 878,76
  (road city-loc-10 city-loc-16)
  (= (road-length city-loc-10 city-loc-16) 1 )
  ; 793,194 -> 680,79
  (road city-loc-17 city-loc-10)
  (= (road-length city-loc-17 city-loc-10) 1 )
  ; 680,79 -> 793,194
  (road city-loc-10 city-loc-17)
  (= (road-length city-loc-10 city-loc-17) 1 )
  ; 793,194 -> 630,336
  (road city-loc-17 city-loc-14)
  (= (road-length city-loc-17 city-loc-14) 1 )
  ; 630,336 -> 793,194
  (road city-loc-14 city-loc-17)
  (= (road-length city-loc-14 city-loc-17) 1 )
  ; 793,194 -> 878,76
  (road city-loc-17 city-loc-16)
  (= (road-length city-loc-17 city-loc-16) 1 )
  ; 878,76 -> 793,194
  (road city-loc-16 city-loc-17)
  (= (road-length city-loc-16 city-loc-17) 1 )
  ; 166,870 -> 125,677
  (road city-loc-18 city-loc-5)
  (= (road-length city-loc-18 city-loc-5) 1 )
  ; 125,677 -> 166,870
  (road city-loc-5 city-loc-18)
  (= (road-length city-loc-5 city-loc-18) 1 )
  ; 166,870 -> 66,817
  (road city-loc-18 city-loc-15)
  (= (road-length city-loc-18 city-loc-15) 1 )
  ; 66,817 -> 166,870
  (road city-loc-15 city-loc-18)
  (= (road-length city-loc-15 city-loc-18) 1 )
  ; 833,827 -> 916,940
  (road city-loc-19 city-loc-2)
  (= (road-length city-loc-19 city-loc-2) 1 )
  ; 916,940 -> 833,827
  (road city-loc-2 city-loc-19)
  (= (road-length city-loc-2 city-loc-19) 1 )
  ; 833,827 -> 753,644
  (road city-loc-19 city-loc-11)
  (= (road-length city-loc-19 city-loc-11) 1 )
  ; 753,644 -> 833,827
  (road city-loc-11 city-loc-19)
  (= (road-length city-loc-11 city-loc-19) 1 )
  ; 41,918 -> 125,677
  (road city-loc-20 city-loc-5)
  (= (road-length city-loc-20 city-loc-5) 1 )
  ; 125,677 -> 41,918
  (road city-loc-5 city-loc-20)
  (= (road-length city-loc-5 city-loc-20) 1 )
  ; 41,918 -> 66,817
  (road city-loc-20 city-loc-15)
  (= (road-length city-loc-20 city-loc-15) 1 )
  ; 66,817 -> 41,918
  (road city-loc-15 city-loc-20)
  (= (road-length city-loc-15 city-loc-20) 1 )
  ; 41,918 -> 166,870
  (road city-loc-20 city-loc-18)
  (= (road-length city-loc-20 city-loc-18) 1 )
  ; 166,870 -> 41,918
  (road city-loc-18 city-loc-20)
  (= (road-length city-loc-18 city-loc-20) 1 )
  (at package-1 city-loc-5)
  (at package-2 city-loc-12)
  (at package-3 city-loc-10)
  (at package-4 city-loc-8)
  (at package-5 city-loc-17)
  (at package-6 city-loc-20)
  (at truck-1 city-loc-8)
  (capacity truck-1 capacity-2)
  (at truck-2 city-loc-14)
  (capacity truck-2 capacity-2)
  (at truck-3 city-loc-8)
  (capacity truck-3 capacity-2)
 )
 (:utility 
    (= (at package-1 city-loc-10) 1 ) 
    (= (at package-2 city-loc-19) 1 ) 
    (= (at package-3 city-loc-20) 1 ) 
    (= (at package-4 city-loc-18) 1 ) 
    (= (at package-5 city-loc-13) 1 ) 
    (= (at package-6 city-loc-1) 1 ) 
    (= (at package-2 city-loc-20) 1 ) 
    (= (at package-5 city-loc-11) 1 ) 
    (= (at package-2 city-loc-1) 1 ) 
    (= (at package-2 city-loc-10) 1 ) 
    (= (at package-4 city-loc-14) 1 ) 
    (= (at package-5 city-loc-17) 1 ) 
    (= (in package-6 truck-3) 1 ) 
    (= (capacity truck-2 capacity-0) 1 ) 
    (= (at truck-2 city-loc-3) 1 ) 
    (= (at package-3 city-loc-17) 1 ) 
 )
 (:bound 744)
 (:use-cost-metric)
 )
 
 


 