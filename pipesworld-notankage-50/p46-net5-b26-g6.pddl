
(define (problem network5new_all_26_6_instance)
  (:domain pipesworld_strips)
  (:objects

    	B10 B21 B17 B14 B22 B4 B6 B15 B19 B20 B13 B8 B2 B11 B24 B5 B0 B1 B25 B18 B7 B12 B9 B3 B23 B16 - batch-atom
	A1 A2 A3 A4 A5 - area
	S12 S13 S34 S23 S15 - pipe
	

  )
  (:init

    ;; All pipelines segments are in normal state
    		(normal S12)
		(normal S13)
		(normal S34)
		(normal S23)
		(normal S15)

    ;; Interfaces restrictions
    	(may-interface lco lco)
	(may-interface gasoleo gasoleo)
	(may-interface rat-a rat-a)
	(may-interface oca1 oca1)
	(may-interface oc1b oc1b)
	(may-interface lco gasoleo)
	(may-interface gasoleo lco)
	(may-interface lco oca1)
	(may-interface oca1 lco)
	(may-interface lco oc1b)
	(may-interface oc1b lco)
	(may-interface lco rat-a)
	(may-interface rat-a lco)
	(may-interface gasoleo rat-a)
	(may-interface rat-a gasoleo)
	(may-interface gasoleo oca1)
	(may-interface oca1 gasoleo)
	(may-interface gasoleo oc1b)
	(may-interface oc1b gasoleo)
	(may-interface oca1 oc1b)
	(may-interface oc1b oca1)
	

    ;; Network topology definition
    	(connect A1 A2 S12)
	(connect A1 A3 S13)
	(connect A3 A4 S34)
	(connect A2 A3 S23)
	(connect A1 A5 S15)
	

    ;; Batch-atoms products
    	(is-product B10 rat-a)
	(is-product B21 rat-a)
	(is-product B17 gasoleo)
	(is-product B14 oca1)
	(is-product B22 oc1b)
	(is-product B4 lco)
	(is-product B6 gasoleo)
	(is-product B15 rat-a)
	(is-product B19 gasoleo)
	(is-product B20 lco)
	(is-product B13 gasoleo)
	(is-product B8 lco)
	(is-product B2 gasoleo)
	(is-product B11 rat-a)
	(is-product B24 oca1)
	(is-product B5 rat-a)
	(is-product B0 lco)
	(is-product B1 lco)
	(is-product B25 lco)
	(is-product B18 oca1)
	(is-product B7 rat-a)
	(is-product B12 lco)
	(is-product B9 lco)
	(is-product B3 oca1)
	(is-product B23 oc1b)
	(is-product B16 oc1b)
	

    ;; Batch-atoms initially located in areas
    	(on B10 A3)
	(on B21 A2)
	(on B17 A5)
	(on B4 A2)
	(on B19 A4)
	(on B8 A1)
	(on B11 A5)
	(on B5 A2)
	(on B0 A3)
	(on B1 A2)
	(on B25 A3)
	(on B9 A5)
	(on B23 A1)
	(on B16 A1)
	

    ;; Batch-atoms initially located in pipes
    	(first B2 S12)
	(follow B6 B2)
	(last B6 S12)
	(first B15 S13)
	(follow B12 B15)
	(last B12 S13)
	(first B22 S34)
	(last B22 S34)
	(first B7 S23)
	(follow B18 B7)
	(follow B20 B18)
	(last B20 S23)
	(first B24 S15)
	(follow B13 B24)
	(follow B14 B13)
	(follow B3 B14)
	(last B3 S15)
	
    ;; Unitary pipeline segments
    		(not-unitary S12)
		(not-unitary S13)
		(unitary S34)
		(not-unitary S23)
		(not-unitary S15)

  )
 (:utility 
    (= (on B17 A4) 1 ) 
    (= (on B6 A3) 1 ) 
    (= (on B20 A2) 1 ) 
    (= (on B8 A3) 1 ) 
    (= (on B12 A5) 1 ) 
    (= (on B9 A1) 1 ) 
    (= (follow b15 b20) 1 ) 
    (= (follow b21 b4) 1 ) 
    (= (follow b23 b1) 1 ) 
    (= (follow b9 b7) 1 ) 
    (= (first b22 s23) 1 ) 
    (= (first b20 s34) 1 ) 
    (= (last b16 s23) 1 ) 
    (= (last b0 s12) 1 ) 
    (= (last b13 s23) 1 ) 
    (= (follow b13 b2) 1 ) 
    (= (follow b8 b17) 1 ) 
    (= (follow b1 b10) 1 ) 
    (= (on b18 a1) 1 ) 
    (= (follow b18 b25) 1 ) 
    (= (on b25 a2) 1 ) 
    (= (follow b13 b15) 1 ) 
    (= (follow b20 b5) 1 ) 
    (= (first b7 s15) 1 ) 
    (= (last b1 s23) 1 ) 
    (= (on b22 a2) 1 ) 
    (= (last b20 s13) 1 ) 
    (= (first b8 s13) 1 ) 
    (= (follow b23 b12) 1 ) 
    (= (follow b16 b23) 1 ) 
    (= (last b13 s34) 1 ) 
    (= (first b13 s12) 1 ) 
    (= (follow b12 b15) 1 ) 
    (= (follow b24 b22) 1 ) 
    (= (last b5 s23) 1 ) 
    (= (last b11 s13) 1 ) 
    (= (follow b4 b16) 1 ) 
    (= (push-updating s12) 1 ) 
    (= (first b10 s23) 1 ) 
    (= (follow b5 b7) 1 ) 
    (= (last b9 s12) 1 ) 
    (= (follow b3 b1) 1 ) 
    (= (last b5 s15) 1 ) 
    (= (follow b2 b8) 1 ) 
    (= (first b10 s15) 1 ) 
    (= (first b23 s13) 1 ) 
    (= (follow b14 b0) 1 ) 
    (= (follow b18 b14) 1 ) 
    (= (follow b19 b24) 1 ) 
    (= (first b19 s34) 1 ) 
    (= (on b5 a3) 1 ) 
    (= (follow b19 b22) 1 ) 
    (= (first b19 s12) 1 ) 
    (= (last b11 s23) 1 ) 
    (= (follow b12 b21) 1 ) 
 )
 (:bound 11)
 )
 


  
