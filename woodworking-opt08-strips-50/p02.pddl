; woodworking task with 4 parts and 140% wood
; Machines:
;   1 grinder
;   1 glazer
;   1 immersion-varnisher
;   1 planer
;   1 highspeed-saw
;   1 spray-varnisher
;   1 saw
; random seed: 196950

(define (problem wood-prob)
  (:domain woodworking)
  (:objects
    grinder0 - grinder
    glazer0 - glazer
    immersion-varnisher0 - immersion-varnisher
    planer0 - planer
    highspeed-saw0 - highspeed-saw
    spray-varnisher0 - spray-varnisher
    saw0 - saw
    green white mauve - acolour
    cherry beech - awood
    p0 p1 p2 p3 - part
    b0 - board
    s0 s1 s2 s3 s4 s5 - aboardsize
  )
  (:init
    (grind-treatment-change varnished colourfragments)
    (grind-treatment-change glazed untreated)
    (grind-treatment-change untreated untreated)
    (grind-treatment-change colourfragments untreated)
    (is-smooth smooth)
    (is-smooth verysmooth)
    (= (total-cost) 1 )
    (boardsize-successor s0 s1)
    (boardsize-successor s1 s2)
    (boardsize-successor s2 s3)
    (boardsize-successor s3 s4)
    (boardsize-successor s4 s5)
    (has-colour glazer0 mauve)
    (has-colour glazer0 white)
    (has-colour immersion-varnisher0 mauve)
    (has-colour immersion-varnisher0 white)
    (has-colour immersion-varnisher0 natural)
    (empty highspeed-saw0)
    (has-colour spray-varnisher0 mauve)
    (has-colour spray-varnisher0 white)
    (has-colour spray-varnisher0 natural)
    (available p0)
    (colour p0 natural)
    (wood p0 cherry)
    (surface-condition p0 rough)
    (treatment p0 varnished)
    (goalsize p0 medium)
    (= (spray-varnish-cost p0) 1 )
    (= (glaze-cost p0) 1 )
    (= (grind-cost p0) 1 )
    (= (plane-cost p0) 1 )
    (available p1)
    (colour p1 green)
    (wood p1 cherry)
    (surface-condition p1 rough)
    (treatment p1 colourfragments)
    (goalsize p1 large)
    (= (spray-varnish-cost p1) 1 )
    (= (glaze-cost p1) 1 )
    (= (grind-cost p1) 1 )
    (= (plane-cost p1) 1 )
    (available p2)
    (colour p2 white)
    (wood p2 cherry)
    (surface-condition p2 verysmooth)
    (treatment p2 glazed)
    (goalsize p2 small)
    (= (spray-varnish-cost p2) 1 )
    (= (glaze-cost p2) 1 )
    (= (grind-cost p2) 1 )
    (= (plane-cost p2) 1 )
    (unused p3)
    (goalsize p3 large)
    (= (spray-varnish-cost p3) 1 )
    (= (glaze-cost p3) 1 )
    (= (grind-cost p3) 1 )
    (= (plane-cost p3) 1 )
    (boardsize b0 s5)
    (wood b0 beech)
    (surface-condition b0 rough)
    (available b0)
  )
 (:utility 
    (= (available p0) 1 ) 
    (= (colour p0 mauve) 1 ) 
    (= (wood p0 cherry) 1 ) 
    (= (surface-condition p0 smooth) 1 ) 
    (= (treatment p0 glazed) 1 ) 
    (= (available p1) 1 ) 
    (= (colour p1 natural) 1 ) 
    (= (wood p1 cherry) 1 ) 
    (= (surface-condition p1 verysmooth) 1 ) 
    (= (treatment p1 varnished) 1 ) 
    (= (available p2) 1 ) 
    (= (colour p2 mauve) 1 ) 
    (= (surface-condition p2 smooth) 1 ) 
    (= (available p3) 1 ) 
    (= (colour p3 white) 1 ) 
    (= (wood p3 beech) 1 ) 
    (= (surface-condition p0 verysmooth) 1 ) 
    (= (colour p1 white) 1 ) 
 )
 (:bound 92)
 (:use-cost-metric)
 )
 
 


  
