; woodworking task with 4 parts and 120% wood
; Machines:
;   1 grinder
;   1 glazer
;   1 immersion-varnisher
;   1 planer
;   1 highspeed-saw
;   1 spray-varnisher
;   1 saw
; random seed: 414413

(define (problem wood-prob)
  (:domain woodworking)
  (:objects
    grinder0 - grinder
    glazer0 - glazer
    immersion-varnisher0 - immersion-varnisher
    planer0 - planer
    highspeed-saw0 - highspeed-saw
    spray-varnisher0 - spray-varnisher
    saw0 - saw
    blue mauve white - acolour
    pine oak - awood
    p0 p1 p2 p3 - part
    b0 b1 - board
    s0 s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 - aboardsize
  )
  (:init
    (grind-treatment-change varnished colourfragments)
    (grind-treatment-change glazed untreated)
    (grind-treatment-change untreated untreated)
    (grind-treatment-change colourfragments untreated)
    (is-smooth smooth)
    (is-smooth verysmooth)
    (= (total-cost) 1 )
    (boardsize-successor s0 s1)
    (boardsize-successor s1 s2)
    (boardsize-successor s2 s3)
    (boardsize-successor s3 s4)
    (boardsize-successor s4 s5)
    (boardsize-successor s5 s6)
    (boardsize-successor s6 s7)
    (boardsize-successor s7 s8)
    (boardsize-successor s8 s9)
    (boardsize-successor s9 s10)
    (has-colour glazer0 blue)
    (has-colour glazer0 mauve)
    (has-colour glazer0 white)
    (has-colour immersion-varnisher0 blue)
    (has-colour immersion-varnisher0 mauve)
    (empty highspeed-saw0)
    (has-colour spray-varnisher0 blue)
    (has-colour spray-varnisher0 mauve)
    (unused p0)
    (goalsize p0 medium)
    (= (spray-varnish-cost p0) 1 )
    (= (glaze-cost p0) 1 )
    (= (grind-cost p0) 1 )
    (= (plane-cost p0) 1 )
    (unused p1)
    (goalsize p1 large)
    (= (spray-varnish-cost p1) 1 )
    (= (glaze-cost p1) 1 )
    (= (grind-cost p1) 1 )
    (= (plane-cost p1) 1 )
    (unused p2)
    (goalsize p2 large)
    (= (spray-varnish-cost p2) 1 )
    (= (glaze-cost p2) 1 )
    (= (grind-cost p2) 1 )
    (= (plane-cost p2) 1 )
    (unused p3)
    (goalsize p3 medium)
    (= (spray-varnish-cost p3) 1 )
    (= (glaze-cost p3) 1 )
    (= (grind-cost p3) 1 )
    (= (plane-cost p3) 1 )
    (boardsize b0 s10)
    (wood b0 oak)
    (surface-condition b0 rough)
    (available b0)
    (boardsize b1 s3)
    (wood b1 pine)
    (surface-condition b1 rough)
    (available b1)
  )
 (:utility 
    (= (available p0) 1 ) 
    (= (colour p0 mauve) 1 ) 
    (= (surface-condition p0 smooth) 1 ) 
    (= (available p1) 1 ) 
    (= (colour p1 blue) 1 ) 
    (= (surface-condition p1 smooth) 1 ) 
    (= (available p2) 1 ) 
    (= (colour p2 white) 1 ) 
    (= (wood p2 oak) 1 ) 
    (= (surface-condition p2 smooth) 1 ) 
    (= (treatment p2 glazed) 1 ) 
    (= (available p3) 1 ) 
    (= (colour p3 mauve) 1 ) 
    (= (wood p3 pine) 1 ) 
    (= (treatment p3 glazed) 1 ) 
    (= (surface-condition p2 verysmooth) 1 ) 
    (= (surface-condition p3 rough) 1 ) 
    (= (surface-condition p3 smooth) 1 ) 
 )
 (:bound 112)
 (:use-cost-metric)
 )
 
 


  
