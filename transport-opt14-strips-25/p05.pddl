; Transport city-sequential-18nodes-1000size-4degree-100mindistance-3trucks-6packages-2014seed

(define (problem transport-city-sequential-18nodes-1000size-4degree-100mindistance-3trucks-6packages-2014seed)
 (:domain transport)
 (:objects
  city-loc-1 - location
  city-loc-2 - location
  city-loc-3 - location
  city-loc-4 - location
  city-loc-5 - location
  city-loc-6 - location
  city-loc-7 - location
  city-loc-8 - location
  city-loc-9 - location
  city-loc-10 - location
  city-loc-11 - location
  city-loc-12 - location
  city-loc-13 - location
  city-loc-14 - location
  city-loc-15 - location
  city-loc-16 - location
  city-loc-17 - location
  city-loc-18 - location
  truck-1 - vehicle
  truck-2 - vehicle
  truck-3 - vehicle
  package-1 - package
  package-2 - package
  package-3 - package
  package-4 - package
  package-5 - package
  package-6 - package
  capacity-0 - capacity-number
  capacity-1 - capacity-number
  capacity-2 - capacity-number
  capacity-3 - capacity-number
  capacity-4 - capacity-number
 )
 (:init
  (= (total-cost) 1 )
  (capacity-predecessor capacity-0 capacity-1)
  (capacity-predecessor capacity-1 capacity-2)
  (capacity-predecessor capacity-2 capacity-3)
  (capacity-predecessor capacity-3 capacity-4)
  ; 560,131 -> 285,216
  (road city-loc-3 city-loc-1)
  (= (road-length city-loc-3 city-loc-1) 1 )
  ; 285,216 -> 560,131
  (road city-loc-1 city-loc-3)
  (= (road-length city-loc-1 city-loc-3) 1 )
  ; 181,38 -> 285,216
  (road city-loc-5 city-loc-1)
  (= (road-length city-loc-5 city-loc-1) 1 )
  ; 285,216 -> 181,38
  (road city-loc-1 city-loc-5)
  (= (road-length city-loc-1 city-loc-5) 1 )
  ; 141,876 -> 251,677
  (road city-loc-6 city-loc-4)
  (= (road-length city-loc-6 city-loc-4) 1 )
  ; 251,677 -> 141,876
  (road city-loc-4 city-loc-6)
  (= (road-length city-loc-4 city-loc-6) 1 )
  ; 239,307 -> 285,216
  (road city-loc-8 city-loc-1)
  (= (road-length city-loc-8 city-loc-1) 1 )
  ; 285,216 -> 239,307
  (road city-loc-1 city-loc-8)
  (= (road-length city-loc-1 city-loc-8) 1 )
  ; 239,307 -> 181,38
  (road city-loc-8 city-loc-5)
  (= (road-length city-loc-8 city-loc-5) 1 )
  ; 181,38 -> 239,307
  (road city-loc-5 city-loc-8)
  (= (road-length city-loc-5 city-loc-8) 1 )
  ; 118,400 -> 285,216
  (road city-loc-9 city-loc-1)
  (= (road-length city-loc-9 city-loc-1) 1 )
  ; 285,216 -> 118,400
  (road city-loc-1 city-loc-9)
  (= (road-length city-loc-1 city-loc-9) 1 )
  ; 118,400 -> 251,677
  (road city-loc-9 city-loc-4)
  (= (road-length city-loc-9 city-loc-4) 1 )
  ; 251,677 -> 118,400
  (road city-loc-4 city-loc-9)
  (= (road-length city-loc-4 city-loc-9) 1 )
  ; 118,400 -> 239,307
  (road city-loc-9 city-loc-8)
  (= (road-length city-loc-9 city-loc-8) 1 )
  ; 239,307 -> 118,400
  (road city-loc-8 city-loc-9)
  (= (road-length city-loc-8 city-loc-9) 1 )
  ; 340,7 -> 285,216
  (road city-loc-10 city-loc-1)
  (= (road-length city-loc-10 city-loc-1) 1 )
  ; 285,216 -> 340,7
  (road city-loc-1 city-loc-10)
  (= (road-length city-loc-1 city-loc-10) 1 )
  ; 340,7 -> 560,131
  (road city-loc-10 city-loc-3)
  (= (road-length city-loc-10 city-loc-3) 1 )
  ; 560,131 -> 340,7
  (road city-loc-3 city-loc-10)
  (= (road-length city-loc-3 city-loc-10) 1 )
  ; 340,7 -> 181,38
  (road city-loc-10 city-loc-5)
  (= (road-length city-loc-10 city-loc-5) 1 )
  ; 181,38 -> 340,7
  (road city-loc-5 city-loc-10)
  (= (road-length city-loc-5 city-loc-10) 1 )
  ; 340,7 -> 239,307
  (road city-loc-10 city-loc-8)
  (= (road-length city-loc-10 city-loc-8) 1 )
  ; 239,307 -> 340,7
  (road city-loc-8 city-loc-10)
  (= (road-length city-loc-8 city-loc-10) 1 )
  ; 675,345 -> 895,506
  (road city-loc-11 city-loc-2)
  (= (road-length city-loc-11 city-loc-2) 1 )
  ; 895,506 -> 675,345
  (road city-loc-2 city-loc-11)
  (= (road-length city-loc-2 city-loc-11) 1 )
  ; 675,345 -> 560,131
  (road city-loc-11 city-loc-3)
  (= (road-length city-loc-11 city-loc-3) 1 )
  ; 560,131 -> 675,345
  (road city-loc-3 city-loc-11)
  (= (road-length city-loc-3 city-loc-11) 1 )
  ; 973,757 -> 895,506
  (road city-loc-12 city-loc-2)
  (= (road-length city-loc-12 city-loc-2) 1 )
  ; 895,506 -> 973,757
  (road city-loc-2 city-loc-12)
  (= (road-length city-loc-2 city-loc-12) 1 )
  ; 866,797 -> 895,506
  (road city-loc-13 city-loc-2)
  (= (road-length city-loc-13 city-loc-2) 1 )
  ; 895,506 -> 866,797
  (road city-loc-2 city-loc-13)
  (= (road-length city-loc-2 city-loc-13) 1 )
  ; 866,797 -> 640,921
  (road city-loc-13 city-loc-7)
  (= (road-length city-loc-13 city-loc-7) 1 )
  ; 640,921 -> 866,797
  (road city-loc-7 city-loc-13)
  (= (road-length city-loc-7 city-loc-13) 1 )
  ; 866,797 -> 973,757
  (road city-loc-13 city-loc-12)
  (= (road-length city-loc-13 city-loc-12) 1 )
  ; 973,757 -> 866,797
  (road city-loc-12 city-loc-13)
  (= (road-length city-loc-12 city-loc-13) 1 )
  ; 436,867 -> 251,677
  (road city-loc-14 city-loc-4)
  (= (road-length city-loc-14 city-loc-4) 1 )
  ; 251,677 -> 436,867
  (road city-loc-4 city-loc-14)
  (= (road-length city-loc-4 city-loc-14) 1 )
  ; 436,867 -> 141,876
  (road city-loc-14 city-loc-6)
  (= (road-length city-loc-14 city-loc-6) 1 )
  ; 141,876 -> 436,867
  (road city-loc-6 city-loc-14)
  (= (road-length city-loc-6 city-loc-14) 1 )
  ; 436,867 -> 640,921
  (road city-loc-14 city-loc-7)
  (= (road-length city-loc-14 city-loc-7) 1 )
  ; 640,921 -> 436,867
  (road city-loc-7 city-loc-14)
  (= (road-length city-loc-7 city-loc-14) 1 )
  ; 822,267 -> 895,506
  (road city-loc-15 city-loc-2)
  (= (road-length city-loc-15 city-loc-2) 1 )
  ; 895,506 -> 822,267
  (road city-loc-2 city-loc-15)
  (= (road-length city-loc-2 city-loc-15) 1 )
  ; 822,267 -> 560,131
  (road city-loc-15 city-loc-3)
  (= (road-length city-loc-15 city-loc-3) 1 )
  ; 560,131 -> 822,267
  (road city-loc-3 city-loc-15)
  (= (road-length city-loc-3 city-loc-15) 1 )
  ; 822,267 -> 675,345
  (road city-loc-15 city-loc-11)
  (= (road-length city-loc-15 city-loc-11) 1 )
  ; 675,345 -> 822,267
  (road city-loc-11 city-loc-15)
  (= (road-length city-loc-11 city-loc-15) 1 )
  ; 599,455 -> 895,506
  (road city-loc-16 city-loc-2)
  (= (road-length city-loc-16 city-loc-2) 1 )
  ; 895,506 -> 599,455
  (road city-loc-2 city-loc-16)
  (= (road-length city-loc-2 city-loc-16) 1 )
  ; 599,455 -> 675,345
  (road city-loc-16 city-loc-11)
  (= (road-length city-loc-16 city-loc-11) 1 )
  ; 675,345 -> 599,455
  (road city-loc-11 city-loc-16)
  (= (road-length city-loc-11 city-loc-16) 1 )
  ; 599,455 -> 822,267
  (road city-loc-16 city-loc-15)
  (= (road-length city-loc-16 city-loc-15) 1 )
  ; 822,267 -> 599,455
  (road city-loc-15 city-loc-16)
  (= (road-length city-loc-15 city-loc-16) 1 )
  ; 370,762 -> 251,677
  (road city-loc-17 city-loc-4)
  (= (road-length city-loc-17 city-loc-4) 1 )
  ; 251,677 -> 370,762
  (road city-loc-4 city-loc-17)
  (= (road-length city-loc-4 city-loc-17) 1 )
  ; 370,762 -> 141,876
  (road city-loc-17 city-loc-6)
  (= (road-length city-loc-17 city-loc-6) 1 )
  ; 141,876 -> 370,762
  (road city-loc-6 city-loc-17)
  (= (road-length city-loc-6 city-loc-17) 1 )
  ; 370,762 -> 640,921
  (road city-loc-17 city-loc-7)
  (= (road-length city-loc-17 city-loc-7) 1 )
  ; 640,921 -> 370,762
  (road city-loc-7 city-loc-17)
  (= (road-length city-loc-7 city-loc-17) 1 )
  ; 370,762 -> 436,867
  (road city-loc-17 city-loc-14)
  (= (road-length city-loc-17 city-loc-14) 1 )
  ; 436,867 -> 370,762
  (road city-loc-14 city-loc-17)
  (= (road-length city-loc-14 city-loc-17) 1 )
  ; 209,484 -> 285,216
  (road city-loc-18 city-loc-1)
  (= (road-length city-loc-18 city-loc-1) 1 )
  ; 285,216 -> 209,484
  (road city-loc-1 city-loc-18)
  (= (road-length city-loc-1 city-loc-18) 1 )
  ; 209,484 -> 251,677
  (road city-loc-18 city-loc-4)
  (= (road-length city-loc-18 city-loc-4) 1 )
  ; 251,677 -> 209,484
  (road city-loc-4 city-loc-18)
  (= (road-length city-loc-4 city-loc-18) 1 )
  ; 209,484 -> 239,307
  (road city-loc-18 city-loc-8)
  (= (road-length city-loc-18 city-loc-8) 1 )
  ; 239,307 -> 209,484
  (road city-loc-8 city-loc-18)
  (= (road-length city-loc-8 city-loc-18) 1 )
  ; 209,484 -> 118,400
  (road city-loc-18 city-loc-9)
  (= (road-length city-loc-18 city-loc-9) 1 )
  ; 118,400 -> 209,484
  (road city-loc-9 city-loc-18)
  (= (road-length city-loc-9 city-loc-18) 1 )
  (at package-1 city-loc-3)
  (at package-2 city-loc-10)
  (at package-3 city-loc-13)
  (at package-4 city-loc-15)
  (at package-5 city-loc-10)
  (at package-6 city-loc-11)
  (at truck-1 city-loc-4)
  (capacity truck-1 capacity-4)
  (at truck-2 city-loc-4)
  (capacity truck-2 capacity-4)
  (at truck-3 city-loc-17)
  (capacity truck-3 capacity-2)
 )
 (:utility 
    (= (at package-1 city-loc-6) 1 ) 
    (= (at package-2 city-loc-1) 1 ) 
    (= (at package-3 city-loc-8) 1 ) 
    (= (at package-4 city-loc-6) 1 ) 
    (= (at package-5 city-loc-16) 1 ) 
    (= (at package-6 city-loc-14) 1 ) 
    (= (capacity truck-2 capacity-1) 1 ) 
    (= (at package-3 city-loc-11) 1 ) 
    (= (at package-2 city-loc-14) 1 ) 
    (= (in package-2 truck-1) 1 ) 
    (= (in package-4 truck-3) 1 ) 
    (= (at package-5 city-loc-2) 1 ) 
    (= (capacity truck-1 capacity-3) 1 ) 
    (= (at package-5 city-loc-3) 1 ) 
    (= (at package-6 city-loc-8) 1 ) 
 )
 (:bound 186)
 (:use-cost-metric)
 )
 
 


 
