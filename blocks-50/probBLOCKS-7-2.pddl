(define (problem BLOCKS-7-2)
(:domain BLOCKS)
(:objects E G C D F A B )
(:INIT (CLEAR B) (CLEAR A) (ONTABLE F) (ONTABLE D) (ON B C) (ON C G) (ON G E)
 (ON E F) (ON A D) (HANDEMPTY))
 (:utility 
    (= (ON E B) 1 ) 
    (= (ON B F) 1 ) 
    (= (ON F D) 1 ) 
    (= (ON D A) 1 ) 
    (= (ON A C) 1 ) 
    (= (ON C G) 1 ) 
    (= (holding g) 1 ) 
    (= (on d g) 1 ) 
    (= (on d e) 1 ) 
 )
 (:bound 10)
 )
 


