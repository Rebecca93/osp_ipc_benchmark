(define (problem BLOCKS-10-0)
(:domain BLOCKS)
(:objects D A H G B J E I F C )
(:INIT (CLEAR C) (CLEAR F) (ONTABLE I) (ONTABLE F) (ON C E) (ON E J) (ON J B)
 (ON B G) (ON G H) (ON H A) (ON A D) (ON D I) (HANDEMPTY))
 (:utility 
    (= (ON D C) 1 ) 
    (= (ON C F) 1 ) 
    (= (ON F J) 1 ) 
    (= (ON J E) 1 ) 
    (= (ON E H) 1 ) 
    (= (ON H B) 1 ) 
    (= (ON B A) 1 ) 
    (= (ON A G) 1 ) 
    (= (ON G I) 1 ) 
    (= (on h b) 1 ) 
    (= (on c b) 1 ) 
    (= (clear a) 1 ) 
    (= (on c a) 1 ) 
    (= (on c e) 1 ) 
    (= (on d d) 1 ) 
 )
 (:bound 17)
 )
 


