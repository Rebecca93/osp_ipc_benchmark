(define (problem BLOCKS-8-1)
(:domain BLOCKS)
(:objects B A G C F D H E )
(:INIT (CLEAR E) (CLEAR H) (CLEAR D) (CLEAR F) (ONTABLE C) (ONTABLE G)
 (ONTABLE D) (ONTABLE F) (ON E C) (ON H A) (ON A B) (ON B G) (HANDEMPTY))
 (:utility 
    (= (ON C D) 1 ) 
    (= (ON D B) 1 ) 
    (= (ON B G) 1 ) 
    (= (ON G F) 1 ) 
    (= (ON F H) 1 ) 
    (= (ON H A) 1 ) 
    (= (ON A E) 1 ) 
    (= (on e e) 1 ) 
    (= (on f e) 1 ) 
    (= (on g h) 1 ) 
    (= (on f g) 1 ) 
 )
 (:bound 10)
 )
 


