(define (problem BLOCKS-4-1)
(:domain BLOCKS)
(:objects A C D B )
(:INIT (CLEAR B) (ONTABLE D) (ON B C) (ON C A) (ON A D) (HANDEMPTY))
 (:utility 
    (= (ON D C) 1 ) 
    (= (ON C A) 1 ) 
    (= (ON A B) 1 ) 
    (= (holding c) 1 ) 
 )
 (:bound 5)
 )
 


