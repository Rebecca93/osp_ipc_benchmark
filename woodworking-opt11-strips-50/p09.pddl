; woodworking task with 7 parts and 140% wood
; Machines:
;   1 grinder
;   1 glazer
;   1 immersion-varnisher
;   1 planer
;   1 highspeed-saw
;   1 spray-varnisher
;   1 saw
; random seed: 42617

(define (problem wood-prob)
  (:domain woodworking)
  (:objects
    grinder0 - grinder
    glazer0 - glazer
    immersion-varnisher0 - immersion-varnisher
    planer0 - planer
    highspeed-saw0 - highspeed-saw
    spray-varnisher0 - spray-varnisher
    saw0 - saw
    blue red white green black - acolour
    mahogany teak - awood
    p0 p1 p2 p3 p4 p5 p6 - part
    b0 b1 b2 - board
    s0 s1 s2 s3 s4 s5 s6 s7 s8 - aboardsize
  )
  (:init
    (grind-treatment-change varnished colourfragments)
    (grind-treatment-change glazed untreated)
    (grind-treatment-change untreated untreated)
    (grind-treatment-change colourfragments untreated)
    (is-smooth smooth)
    (is-smooth verysmooth)
    (= (total-cost) 1 )
    (boardsize-successor s0 s1)
    (boardsize-successor s1 s2)
    (boardsize-successor s2 s3)
    (boardsize-successor s3 s4)
    (boardsize-successor s4 s5)
    (boardsize-successor s5 s6)
    (boardsize-successor s6 s7)
    (boardsize-successor s7 s8)
    (has-colour glazer0 blue)
    (has-colour glazer0 green)
    (has-colour glazer0 natural)
    (has-colour glazer0 black)
    (has-colour immersion-varnisher0 black)
    (has-colour immersion-varnisher0 red)
    (empty highspeed-saw0)
    (has-colour spray-varnisher0 black)
    (has-colour spray-varnisher0 red)
    (unused p0)
    (goalsize p0 large)
    (= (spray-varnish-cost p0) 1 )
    (= (glaze-cost p0) 1 )
    (= (grind-cost p0) 1 )
    (= (plane-cost p0) 1 )
    (unused p1)
    (goalsize p1 large)
    (= (spray-varnish-cost p1) 1 )
    (= (glaze-cost p1) 1 )
    (= (grind-cost p1) 1 )
    (= (plane-cost p1) 1 )
    (unused p2)
    (goalsize p2 small)
    (= (spray-varnish-cost p2) 1 )
    (= (glaze-cost p2) 1 )
    (= (grind-cost p2) 1 )
    (= (plane-cost p2) 1 )
    (unused p3)
    (goalsize p3 small)
    (= (spray-varnish-cost p3) 1 )
    (= (glaze-cost p3) 1 )
    (= (grind-cost p3) 1 )
    (= (plane-cost p3) 1 )
    (unused p4)
    (goalsize p4 small)
    (= (spray-varnish-cost p4) 1 )
    (= (glaze-cost p4) 1 )
    (= (grind-cost p4) 1 )
    (= (plane-cost p4) 1 )
    (available p5)
    (colour p5 white)
    (wood p5 teak)
    (surface-condition p5 rough)
    (treatment p5 varnished)
    (goalsize p5 large)
    (= (spray-varnish-cost p5) 1 )
    (= (glaze-cost p5) 1 )
    (= (grind-cost p5) 1 )
    (= (plane-cost p5) 1 )
    (unused p6)
    (goalsize p6 medium)
    (= (spray-varnish-cost p6) 1 )
    (= (glaze-cost p6) 1 )
    (= (grind-cost p6) 1 )
    (= (plane-cost p6) 1 )
    (boardsize b0 s3)
    (wood b0 teak)
    (surface-condition b0 rough)
    (available b0)
    (boardsize b1 s8)
    (wood b1 mahogany)
    (surface-condition b1 rough)
    (available b1)
    (boardsize b2 s5)
    (wood b2 mahogany)
    (surface-condition b2 smooth)
    (available b2)
  )
 (:utility 
    (= (available p0) 1 ) 
    (= (wood p0 mahogany) 1 ) 
    (= (treatment p0 glazed) 1 ) 
    (= (available p1) 1 ) 
    (= (surface-condition p1 smooth) 1 ) 
    (= (treatment p1 varnished) 1 ) 
    (= (available p2) 1 ) 
    (= (colour p2 green) 1 ) 
    (= (wood p2 teak) 1 ) 
    (= (treatment p2 glazed) 1 ) 
    (= (available p3) 1 ) 
    (= (colour p3 blue) 1 ) 
    (= (wood p3 mahogany) 1 ) 
    (= (surface-condition p3 verysmooth) 1 ) 
    (= (treatment p3 glazed) 1 ) 
    (= (available p4) 1 ) 
    (= (colour p4 red) 1 ) 
    (= (wood p4 teak) 1 ) 
    (= (surface-condition p4 smooth) 1 ) 
    (= (treatment p4 varnished) 1 ) 
    (= (available p5) 1 ) 
    (= (colour p5 natural) 1 ) 
    (= (treatment p5 glazed) 1 ) 
    (= (available p6) 1 ) 
    (= (colour p6 black) 1 ) 
    (= (wood p6 mahogany) 1 ) 
    (= (colour p5 blue) 1 ) 
    (= (colour p1 red) 1 ) 
    (= (surface-condition b0 rough) 1 ) 
    (= (colour p1 green) 1 ) 
    (= (unused p1) 1 ) 
 )
 (:bound 135)
 (:use-cost-metric)
 )
 
 


  
