(define (problem BLOCKS-7-1)
(:domain BLOCKS)
(:objects E B D F G C A )
(:INIT (CLEAR A) (CLEAR C) (ONTABLE G) (ONTABLE F) (ON A G) (ON C D) (ON D B)
 (ON B E) (ON E F) (HANDEMPTY))
 (:utility 
    (= (ON A E) 1 ) 
    (= (ON E B) 1 ) 
    (= (ON B F) 1 ) 
    (= (ON F G) 1 ) 
    (= (ON G C) 1 ) 
    (= (ON C D) 1 ) 
    (= (on c c) 1 ) 
    (= (on a a) 1 ) 
    (= (ontable f) 1 ) 
 )
 (:bound 5)
 )
 


