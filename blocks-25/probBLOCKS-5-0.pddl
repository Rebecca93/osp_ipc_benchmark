(define (problem BLOCKS-5-0)
(:domain BLOCKS)
(:objects B E A C D )
(:INIT (CLEAR D) (CLEAR C) (ONTABLE D) (ONTABLE A) (ON C E) (ON E B) (ON B A)
 (HANDEMPTY))
 (:utility 
    (= (ON A E) 1 ) 
    (= (ON E B) 1 ) 
    (= (ON B D) 1 ) 
    (= (ON D C) 1 ) 
    (= (on e e) 1 ) 
    (= (on d a) 1 ) 
 )
 (:bound 3)
 )
 


