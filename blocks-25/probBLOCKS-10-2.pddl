(define (problem BLOCKS-10-2)
(:domain BLOCKS)
(:objects B G E D F H I A C J )
(:INIT (CLEAR J) (CLEAR C) (ONTABLE A) (ONTABLE C) (ON J I) (ON I H) (ON H F)
 (ON F D) (ON D E) (ON E G) (ON G B) (ON B A) (HANDEMPTY))
 (:utility 
    (= (ON B E) 1 ) 
    (= (ON E I) 1 ) 
    (= (ON I G) 1 ) 
    (= (ON G H) 1 ) 
    (= (ON H C) 1 ) 
    (= (ON C A) 1 ) 
    (= (ON A F) 1 ) 
    (= (ON F J) 1 ) 
    (= (ON J D) 1 ) 
    (= (on j h) 1 ) 
    (= (on d e) 1 ) 
    (= (ontable a) 1 ) 
    (= (on c f) 1 ) 
    (= (on h d) 1 ) 
    (= (on j b) 1 ) 
 )
 (:bound 8)
 )
 


